using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Shooju.Tests
{
    class TestRaw : TestBase
    {
        [Test]
        public void TestGetSeriesRaw()
        {
            var queryArgs = new Dictionary<string, object>
            {
                { "series_id", SamplesSeriesId },
                { "date_format", "milli" },
                { "df", "MIN" },
                { "dt", "MAX" },
                { "max_points", 10 },
                { "fields", "*" },
            };

            var response = Connection.RawGet("/series/", queryArgs);
            Assert.IsEmpty(response["error"]?.ToString() ?? "");
        }

        [Test]
        public void TestBlockListRaw()
        {
            var queryArgs = new Dictionary<string, object>
            {
                { "date_format", "milli" },
                { "df", "MIN" },
                { "dt", "MAX" },
                { "max_points", 10 },
                { "fields", "*" },
            };

            var postData = new JObject
            {
                { "series_ids", new JArray(SamplesSeriesId) }
            };

            var response = Connection.RawPost("/series/", queryArgs, postData);
            Assert.IsEmpty(response["error"]?.ToString() ?? "");
        }
    }
}
