﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Shooju.Model;

namespace Shooju.Tests
{
    static class Util
    {
        public static Series GetFullSeries(this Connection conn, string query)
        {
            var res = conn.GetSeries(query, fields: new[] { "*" }, maxPoints: -1);
            return res;
        }
    }
}
