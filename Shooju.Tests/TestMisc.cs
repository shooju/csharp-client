﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using NUnit.Framework;

using Shooju.Example;
using Shooju.Model;
using Shooju.Exceptions;
using Shooju.Utils;

namespace Shooju.Tests
{
    class TestMisc : TestBase
    {
        [SetUp]
        public override void Setup()
        {
            base.Setup();
            Program.InitVals1(Connection, Job, TestPrefix);
        }

        [Test]
        public void TestPing()
        {
            var response = Connection.Ping();
            Assert.IsTrue(response);
        }

        [Test]
        public void TestFacets()
        {
            Thread.Sleep(5000);
            Program.RunGetFacets(Connection, Job, TestPrefix);
        }

        [Test]
        public void TestSeriesList()
        {
            var series1 = $"{TestPrefix}\\A";
            var series2 = $"{TestPrefix}\\B";
            var responses = Connection.GetSeriesList(new[] { series1, series2, "Invalid" }, new string[0], null, null, 20);
            Assert.AreEqual(responses.Count, 3);
            Assert.AreEqual(responses[0].SeriesId, series1);
            Assert.AreEqual(responses[1].SeriesId, series2);
            Assert.IsFalse(responses[2].Success);

            Assert.AreEqual(responses[0].Points.Count, 3);
            Assert.AreEqual(responses[1].Points.Count, 3);

            Assert.AreEqual(responses[0].Points[0].Value, 1000);
            Assert.AreEqual(responses[1].Points[0].Value, 3000);

            Assert.AreEqual(responses[0].Points[0].Date, new DateTime(2015, 1, 1));
            Assert.AreEqual(responses[1].Points[0].Date, new DateTime(2015, 1, 1));

            responses = Connection.GetSeriesList(new[] { series1, series2, "Invalid" }, new string[0], new DateTime(2015, 2, 1), null, 1);
            Assert.AreEqual(responses.Count, 3);
            Assert.AreEqual(responses[0].SeriesId, series1);
            Assert.AreEqual(responses[1].SeriesId, series2);

            Assert.AreEqual(responses[0].Points.Count, 1);
            Assert.AreEqual(responses[1].Points.Count, 1);

            Assert.AreEqual(responses[0].Points[0].Value, 2000);
            Assert.AreEqual(responses[1].Points[0].Value, 4000);

            Assert.AreEqual(responses[0].Points[0].Date, new DateTime(2015, 2, 1));
            Assert.AreEqual(responses[1].Points[0].Date, new DateTime(2015, 2, 1));
        }

        [Test]
        public void TestSeriesScrollSearch()
        {
            Thread.Sleep(5000); //refresh search index

            var responses = Connection.Scroll(string.Format("sid:\"{0}\"", TestPrefix), fields: new[] { "invalid", "fielda" },
                maxPoints: 10, maxSeries: 2, scrollBatchSize: 1).ToList();
            Assert.AreEqual(responses.Count, 2);

            var series1 = $"{TestPrefix}\\A";
            var series2 = $"{TestPrefix}\\B";

            Assert.AreEqual(responses[0].SeriesId, series1);
            Assert.AreEqual(responses[1].SeriesId, series2);

            Assert.AreEqual(responses[0].Points.Count, 3);
            Assert.AreEqual(responses[1].Points.Count, 3);

            Assert.AreEqual(responses[0].Points[0].Value, 1000);
            Assert.AreEqual(responses[1].Points[0].Value, 3000);


            Assert.AreEqual(responses[0].Points[0].Date, new DateTime(2015, 1, 1));
            Assert.AreEqual(responses[1].Points[0].Date, new DateTime(2015, 1, 1));

            Assert.AreEqual(responses[0].Fields.Count, 1);
            Assert.AreEqual(responses[1].Fields.Count, 1);

            Assert.AreEqual(responses[0].Fields["fielda"], "seriesId1");

            // full scroll
            responses = Connection.Scroll($"sid:\"{TestPrefix}\"", fields: new[] { "invalid", "fielda" },
                maxPoints: 10, scrollBatchSize: 1, sort: "sid asc").ToList();
            Assert.AreEqual(responses.Count, 2);

            series1 = $"{TestPrefix}\\A";
            series2 = $"{TestPrefix}\\B";

            Assert.AreEqual(responses[0].SeriesId, series1);
            Assert.AreEqual(responses[1].SeriesId, series2);

            Assert.AreEqual(responses[0].Points.Count, 3);
            Assert.AreEqual(responses[1].Points.Count, 3);

            Assert.AreEqual(responses[0].Points[0].Value, 1000);
            Assert.AreEqual(responses[1].Points[0].Value, 3000);

            Assert.AreEqual(responses[0].Points[0].Date, new DateTime(2015, 1, 1));
            Assert.AreEqual(responses[1].Points[0].Date, new DateTime(2015, 1, 1));

            Assert.AreEqual(responses[0].Fields.Count, 1);
            Assert.AreEqual(responses[1].Fields.Count, 1);

            Assert.AreEqual(responses[0].Fields["fielda"], "seriesId1");

            // invalid query
            Assert.Throws<ShoojuClientException>(delegate
            {
                var items = Connection.Scroll($"sid:\"{TestPrefix}", fields: new[] { "invalid", "fielda" },
                    maxPoints: 10, scrollBatchSize: 1).ToList();
            });
        }

        [Test]
        public void TestDataExport1()
        {
            var responses = Connection.LoadDatesValues("tree:test", null, null, 10).ToArray();
            Assert.IsTrue(responses.Length > 0);
        }

        [Test]
        public void TestDataExport2()
        {
            var responses = Connection.LoadFieldsValues("tree:test", new[] { "*" }).ToArray();
            Assert.IsTrue(responses.Length > 0);
        }

        [Test]
        public void TestGetSeriesPostApi()
        {
            Program.RunGetSeriesPostApi(Connection, Job, TestPrefix);
        }

        [Test]
        public void TestWritePointsAndFields()
        {
            Program.RunWritePointsAndFields(Connection, Job, TestPrefix);
        }

        [Test]
        public void TestWriteReportedPointsAndFields()
        {
            Program.RunWriteReportedPointsAndFields(Connection, Job, TestPrefix);
        }
    }
}

