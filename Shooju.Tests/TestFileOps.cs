﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Shooju.Example;
using Shooju.Utils;

namespace Shooju.Tests
{
    class TestFileOps : TestBase
    {
        private const string DownloadFileId = "95b0e4c2-4ce1-4bdd-9d7d-c698fc89d05f";

        [Test]
        public void TestDownloadFile1()
        {
            var bytes = Connection.DownloadFile(DownloadFileId);
            Assert.IsTrue(bytes.Length > 0);
        }

        [Test]
        public void TestDownloadFile2()
        {
            var fileName = Path.GetTempPath() + @"\ShoojuTest." + Path.GetRandomFileName();
            using (var stream = Connection.DownloadFile(DownloadFileId, fileName))
            {
                var bytes = stream.ReadAllBytes();
                Assert.IsTrue(bytes.Length > 0);
            }
        }

        [Test]
        public void TestDownloadNonExistingFile()
        {
            Program.TestDownloadNonExistingFile(Connection);
        }

        [Test]
        public void TestUploadFile()
        {
            Program.TestUploadFile(Connection, out _);
        }

        [Test]
        public void TestUploadFileMultiPart()
        {
            var fileIdMulti = Program.TestUploadFileMultiPart(Connection, out _);
            Program.TestDownloadFile(Connection, fileIdMulti);
        }

        [Test]
        public void TestUploadAndDownloadFile()
        {
            var fileId = Program.TestUploadFile(Connection, out var fileName1);
            var fileName2 = Program.TestDownloadFile(Connection, fileId);

            using (var stream1 = new FileStream(fileName1, FileMode.Open, FileAccess.Read))
            {
                using (var stream2 = new FileStream(fileName2, FileMode.Open, FileAccess.Read))
                {
                    var buf1 = new byte[16 * 1024];
                    var buf2 = new byte[buf1.Length];

                    while (true)
                    {
                        var bytesRead1 = stream1.Read(buf1, 0, buf1.Length);
                        if (bytesRead1 <= 0)
                            break;

                        var bytesRead2 = stream2.Read(buf2, 0, buf2.Length);
                        Assert.AreEqual(bytesRead1, bytesRead2);

                        for (var i = 0; i < bytesRead1; i++)
                        {
                            Assert.AreEqual(buf1[i], buf2[i]);
                        }
                    }
                }
            }
        }
    }
}
