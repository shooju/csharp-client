using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Shooju.Example;
using Shooju.Model;
using Shooju.Utils;

// ReSharper disable PossibleInvalidOperationException
namespace Shooju.Tests
{
    class TestPutAndGet : TestBase
    {
        [SetUp]
        public override void Setup()
        {
            base.Setup();
        }

        [Test]
        public void TestFields()
        {
            var fields = new Dictionary<string, dynamic>(StringComparer.OrdinalIgnoreCase);
            for (var i = 0; i < TestFieldsCount; i++)
            {
                fields.Add($"field{i:D4}", $"{Guid.NewGuid()} {DateTime.UtcNow}");
            }
            Job.Write($"sid={TestPrefix}", fields);
            Job.Submit();
            Connection.RefreshIndex();

            var fieldsRetrieved = Connection.GetFields(TestPrefix);
            Assert.AreEqual(fieldsRetrieved.Count, fields.Count);
            foreach (var pair in fieldsRetrieved)
            {
                Assert.IsTrue(fields.TryGetValue(pair.Key, out var prevVal));
                Assert.AreEqual(prevVal, pair.Value);
            }

            fieldsRetrieved = Connection.GetFields(TestPrefix);

            Assert.AreEqual(fieldsRetrieved.Count, fields.Count);
            foreach (var pair in fieldsRetrieved)
            {
                Assert.IsTrue(fields.TryGetValue(pair.Key, out var prevVal));
                Assert.AreEqual(prevVal, pair.Value);
            }
            fieldsRetrieved = Connection.GetFields(TestPrefix, new List<string> {});
            Assert.AreEqual(fieldsRetrieved.Count, 0);
        }

        [Test]
        public void TestPoints()
        {
            var points = new Dictionary<DateTime, Point>();
            var random = new Random();
            for (var i = 0; i < TestPointsCount; i++)
            {
                var time = new DateTime(1980, 1, 1).AddDays(i);
                points.Add(time, new Point(time, random.NextDouble()));
            }

            Job.Write($"sid={TestPrefix}", points: points.Values.ToArray());
            Job.Submit();
            Connection.RefreshIndex();

            var pointsRetrieved = Connection.GetPoints(TestPrefix, null, null);
            foreach (var point in pointsRetrieved)
            {
                Assert.IsTrue(points.TryGetValue(point.Date, out var prevPoint));
                Assert.AreEqual((double)prevPoint.Value, (double)point.Value, 0.0001);
            }

            pointsRetrieved = Connection.GetPoints(TestPrefix, null, null, 1);
            Assert.AreEqual(pointsRetrieved.Count, 1);
            Assert.AreEqual((double)pointsRetrieved[0].Value, (double)points[new DateTime(1980, 1, 1)].Value, 0.0001);

            pointsRetrieved = Connection.GetPoints(TestPrefix, new DateTime(1980, 1, 2), null, 2);
            Assert.AreEqual(pointsRetrieved.Count, 2);
            Assert.AreEqual((double)pointsRetrieved[0].Value, (double)points[new DateTime(1980, 1, 2)].Value, 0.0001);

            pointsRetrieved = Connection.GetPoints(TestPrefix, null, new DateTime(1980, 1, 3), 2);
            Assert.AreEqual(pointsRetrieved.Count, 2);
            Assert.AreEqual((double)pointsRetrieved[1].Value, (double)points[new DateTime(1980, 1, 2)].Value, 0.0001);

            // get single point
            var p = Connection.GetPoint(TestPrefix, new DateTime(1980, 1, 1));
            Assert.AreEqual((double)p.Value, (double)points[new DateTime(1980, 1, 1)].Value, 0.0001);
            Assert.AreEqual(p.Date, new DateTime(1980, 1, 1));
        }

        [Test]
        public void TestTimezone()
        {
            var points = new Dictionary<DateTime, Point>();
            var random = new Random();
            for (var i = 0; i < 5; i++)
            {
                var time = new DateTime(2022, 1, 1).AddDays(i);
                points.Add(time, new Point(time, random.NextDouble()));
            }

            Job.Write($"sid={TestPrefix}", points: points.Values.ToArray(), timezone: "America/New_York");
            Job.Submit();
            Connection.RefreshIndex();

            var pointsRetrieved = Connection.GetPoints(TestPrefix, null, null);
            foreach (var point in pointsRetrieved)
            {
                Assert.IsTrue(points.TryGetValue(point.Date.AddHours(-5), out var prevPoint));
                Assert.AreEqual((double)prevPoint.Value, (double)point.Value, 0.0001);
            }

        }

        [Test]
        public void TestNonExistingSeries()
        {
            var points = Connection.GetPoints("NonExisting", null, null);
            Assert.IsNull(points);

            var fields = Connection.GetFields("NonExisting");
            Assert.IsNull(fields);

            var point = Connection.GetPoint("NonExisting", new DateTime(1980, 1, 2));
            Assert.IsNull(point);

            point = Connection.GetPoint(TestPrefix, new DateTime(2000, 1, 2));
            Assert.IsNull(point);

            var field = Connection.GetField("NonExisting", "NonExisting");
            Assert.IsNull(field);
        }

        [Test]
        public void TestMultiGet()
        {
            Program.RunMultiGet(Connection, Job, Prefix);
        }

        [Test]
        public void TestGettingMultipleFields()
        {
            var seriesId = TestPrefix + "\\TestGettingMultipleFields";
            Program.RunRemoteJobOperations(Connection, Job, seriesId);
            Program.RunGettingMultipleFields(Connection, seriesId);
        }

        [Test]
        public void TestReplacementAndDeletion()
        {
            Program.RunRemoteJobOperations(Connection, Job, TestPrefix);
            Program.TestSeriesDataReplacing(Connection, Job, TestPrefix);
            Program.TestSeriesDeletion(Connection, Job, TestPrefix);
        }

        [Test]
        public void TestQueryScrolling()
        {
            var seriesId = TestPrefix + "\\TestQueryScrolling";
            Program.RunRemoteJobOperations(Connection, Job, seriesId);
            Program.RunQueryScrolling(Connection, seriesId);
        }

        [Test]
        public void TestReportedPoints()
        {
            Program.RunReportedDates(Connection, Job, Prefix);
        }

        const int TestFieldsCount = 20;
        const int TestPointsCount = 100;
    }
}
