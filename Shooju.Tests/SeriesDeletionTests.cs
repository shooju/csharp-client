﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Shooju.Example;

namespace Shooju.Tests
{
    class SeriesDeletionTests : TestBase
    {
        [Test]
        public void TestDeleteSingle()
        {
            var query = $@"sid={TestPrefix}\TestDeleteSingle";
            Program.InitSampleDataByQuery(Connection, Job, query);
            Job.DeleteSeries(query, true, true);
            Job.Submit();
            Connection.RefreshIndex();
            var res = Connection.GetFullSeries(query);
            Assert.That(res.NotFoundError);
        }

        [Test]
        public void TestDeleteMulti()
        {
            var query = $@"sid={TestPrefix}\TestDeleteMulti";
            Program.InitSampleDataByQuery(Connection, Job, query);
            var deleted = Job.DeleteSeries(query, false, true);
            Assert.That(deleted > 0);
            Connection.RefreshIndex();
            var res = Connection.GetFullSeries(query);
            Assert.That(res.NotFoundError);
        }

        [Test]
        public void TestDeleteReported()
        {
            var query = $@"sid={TestPrefix}\TestDeleteReported";
            var date1 = new DateTime(2000, 1, 1);
            var date2 = new DateTime(2000, 1, 2);
            Job.WriteReported(query, date1, points: Program.SamplePointsData2);
            Job.WriteReported(query, date2, points: Program.SamplePointsData2);
            Job.Submit();
            Connection.RefreshIndex();
            var dates1 = Connection.GetReportedDates(query);
            Assert.AreEqual(2, dates1.Count);

            Job.DeleteReported(query, date2);
            Job.Submit();
            Connection.RefreshIndex();
            var dates2 = Connection.GetReportedDates(query);
            Assert.AreEqual(1, dates2.Count);
        }
    }
}
