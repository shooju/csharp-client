﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using Shooju.Example;

namespace Shooju.Tests
{
    class TestBase
    {
        [SetUp]
        public virtual void Setup()
        {
            Connection = new Connection(Settings.Server, Settings.User, Settings.ApiSecret);
            Prefix = $@"Users\{Settings.User}\IntegrationTestsDotNet\{DateTime.UtcNow:yyyy-MM-dd-HH_mm_ss}";

            TestPrefix = Prefix + @"\" + GetType().Name;

            SamplesSeriesId = Prefix + @"\Samples";
            Job = Connection.RegisterJob(GetType().FullName, 1000);

            CleanupData();
            Program.InitSampleData(Connection, Job, SamplesSeriesId);
        }

        [TearDown]
        public virtual void Cleanup()
        {
            Job?.Finish();
            CleanupData();
        }

        private void CleanupData()
        {
            var deleted = Job.DeleteSeries($"sid:\"{Prefix}\"", false, true);
        }

        protected string Prefix;
        protected string TestPrefix;
        protected string SamplesSeriesId;
        protected Connection Connection;
        protected RemoteJob Job;
    }
}
