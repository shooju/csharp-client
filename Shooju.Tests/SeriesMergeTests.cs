﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using NUnit.Framework;

using Shooju.Example;
using Shooju.Model;
using Shooju.Tools;
using Shooju.Utils;

namespace Shooju.Tests
{
    class SeriesMergeTests
    {
        private static readonly string BasePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath) + @"\SeriesMergeTests\";

        static readonly List<Series> Data0 = new List<Series>
        {
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                }
            },
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                }
            },
        };

        static readonly List<Series> Data1 = new List<Series>
        {
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                }
            },
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                }
            },
            new Series { //15-min localized
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.1),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.1),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.2),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.2),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                }
            },
            new Series { //localized hourly
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                }
            },
            new Series {
                Points = new List<Point>
                {
                    new Point(new DateTime(2013, 11, 02, 2, 0, 0), 2013),
                    new Point(new DateTime(2018, 11, 02, 6, 0, 0), 2018),
                }
            },
            new Series {
                Points = new List<Point>
                {
                    new Point(new DateTime(2013, 11, 02, 2, 0, 0), 2013),
                }
            },
            new Series {
                Points = new List<Point>
                {
                    new Point(new DateTime(2018, 11, 02, 2, 0, 0), 2018),
                }
            },
        };

        static readonly List<Series> Data2 = new List<Series>
        {
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                }
            },
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                }
            },
            new Series { //15-min localized
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.1),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.1),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.2),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.2),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                }
            },
            new Series { //localized hourly
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                }
            },
        };

        static readonly List<Series> Data3 = new List<Series>
        {
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2015, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 5, 0, 0), 5),
                }
            },
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2015, 11, 02, 6, 0, 0), 6),
                }
            },
            new Series { //15-min localized
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.1),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.1),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.2),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.2),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2015, 11, 02, 2, 15, 0), 215.1),
                    new Point(new DateTime(2015, 11, 02, 2, 30, 0), 230.1),
                    new Point(new DateTime(2015, 11, 02, 2, 45, 0), 245.1),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2015, 11, 02, 2, 15, 0), 215.2),
                    new Point(new DateTime(2015, 11, 02, 2, 30, 0), 230.2),
                    new Point(new DateTime(2015, 11, 02, 2, 45, 0), 245.2),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                }
            },
            new Series { //localized hourly
                Points = new List<Point>
                {
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2015, 11, 02, 6, 0, 0), 6),
                }
            },
        };

        static readonly List<Series> Data4 = new List<Series>
        {
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2015, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2015, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                }
            },
            new Series { //non-localized series
                Points = new List<Point>
                {
                    new Point(new DateTime(2015, 11, 02, 6, 0, 0), 6),
                    new Point(new DateTime(2015, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2),
                }
            },
            new Series { //15-min localized
                Points = new List<Point>
                {
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2015, 11, 02, 2, 45, 0), 245.2),
                    new Point(new DateTime(2015, 11, 02, 2, 30, 0), 230.2),
                    new Point(new DateTime(2015, 11, 02, 2, 15, 0), 215.2),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2015, 11, 02, 2, 45, 0), 245.1),
                    new Point(new DateTime(2015, 11, 02, 2, 30, 0), 230.1),
                    new Point(new DateTime(2015, 11, 02, 2, 15, 0), 215.1),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2015, 11, 02, 1, 0, 0), 1),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.2),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.2),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.2),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 2, 45, 0), 245.1),
                    new Point(new DateTime(2014, 11, 02, 2, 30, 0), 230.1),
                    new Point(new DateTime(2014, 11, 02, 2, 15, 0), 215.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 1, 0, 0), 1),
                }
            },
            new Series { //localized hourly
                Points = new List<Point>
                {
                    new Point(new DateTime(2015, 11, 02, 6, 0, 0), 6),
                    new Point(new DateTime(2015, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2015, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3.2),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2015, 11, 02, 3, 0, 0), 3.1),
                    new Point(new DateTime(2015, 11, 02, 2, 0, 0), 2.1),
                    new Point(new DateTime(2014, 11, 02, 6, 0, 0), 6),
                    new Point(new DateTime(2014, 11, 02, 5, 0, 0), 5),
                    new Point(new DateTime(2014, 11, 02, 4, 0, 0), 4),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3.2),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.2),
                    new Point(new DateTime(2014, 11, 02, 3, 0, 0), 3.1),
                    new Point(new DateTime(2014, 11, 02, 2, 0, 0), 2.1),
                }
            },
        };

        private static readonly List<Series> Data5 = new List<Series>
        {
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 10, 29, 0, 0, 0), 1),
                    new Point(new DateTime(2017, 10, 29, 0, 30, 0), 2),
                    new Point(new DateTime(2017, 10, 29, 1, 0, 0), 3.1),
                    new Point(new DateTime(2017, 10, 29, 1, 30, 0), 4.1),
                    new Point(new DateTime(2017, 10, 29, 1, 0, 0), 3.2),
                    new Point(new DateTime(2017, 10, 29, 1, 30, 0), 4.2),
                    new Point(new DateTime(2017, 10, 29, 2, 0, 0), 5),
                    new Point(new DateTime(2017, 10, 29, 2, 30, 0), 6),
                }
            },
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 10, 29, 0, 0, 0), 1),
                    new Point(new DateTime(2017, 10, 29, 1, 0, 0), 3),
                    new Point(new DateTime(2017, 10, 29, 2, 0, 0), 5),
                    new Point(new DateTime(2017, 10, 29, 3, 0, 0), 7),
                    new Point(new DateTime(2017, 10, 29, 4, 0, 0), 8),
                    new Point(new DateTime(2017, 11, 05, 01, 0, 0), 9),
                    new Point(new DateTime(2017, 11, 05, 01, 0, 0), 10),
                }
            },
        };

        private static readonly List<Series> Data6 = new List<Series>
        {
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 30, 00), 1),
                    new Point(new DateTime(2017, 11, 05, 00, 45, 00), 2),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.1),
                    new Point(new DateTime(2017, 11, 05, 01, 15, 00), 4.1),
                    new Point(new DateTime(2017, 11, 05, 01, 30, 00), 5.1),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.2),
                    new Point(new DateTime(2017, 11, 05, 01, 15, 00), 4.2),
                    new Point(new DateTime(2017, 11, 05, 01, 30, 00), 5.5),
                    new Point(new DateTime(2017, 11, 05, 01, 45, 00), 6),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                }
            },
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 00, 00), 0),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.1),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.2),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                    new Point(new DateTime(2017, 11, 05, 03, 00, 00), 8),
                }
            },
        };

        private static readonly List<Series> Data7 = new List<Series>
        {
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 30, 00), 1),
                    new Point(new DateTime(2017, 11, 05, 00, 45, 00), 2),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3),
                    new Point(new DateTime(2017, 11, 05, 01, 15, 00), 4),
                    new Point(new DateTime(2017, 11, 05, 01, 30, 00), 5),
                    new Point(new DateTime(2017, 11, 05, 01, 45, 00), 6),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                }
            },
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 00, 00), 0),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.1),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.2),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                    new Point(new DateTime(2017, 11, 05, 03, 00, 00), 8),
                }
            },
        };

        private static readonly List<Series> Data8 = new List<Series>
        {
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 30, 00), 1),
                    new Point(new DateTime(2017, 11, 05, 00, 45, 00), 2),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3),
                    new Point(new DateTime(2017, 11, 05, 01, 15, 00), 4),
                    new Point(new DateTime(2017, 11, 05, 01, 30, 00), 5),
                    new Point(new DateTime(2017, 11, 05, 01, 45, 00), 6),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                    new Point(new DateTime(2017, 11, 06, 01, 00, 00), 9),
                }
            },
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 00, 00), 0),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.1),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.2),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                    new Point(new DateTime(2017, 11, 05, 03, 00, 00), 8),
                    new Point(new DateTime(2017, 11, 06, 01, 00, 00), 9),
                }
            },
        };

        private static readonly List<Series> Data9 = new List<Series>
        {
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 30, 00), 1),
                    new Point(new DateTime(2017, 11, 05, 00, 45, 00), 2),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3),
                    new Point(new DateTime(2017, 11, 05, 01, 15, 00), 4),
                    new Point(new DateTime(2017, 11, 05, 01, 30, 00), 5),
                    new Point(new DateTime(2017, 11, 05, 01, 45, 00), 6),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                    new Point(new DateTime(2017, 11, 06, 01, 00, 00), 9),
                }
            },
            new Series
            {
            },
            new Series
            {
                Points = new List<Point>
                {
                    new Point(new DateTime(2017, 11, 05, 00, 00, 00), 0),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.1),
                    new Point(new DateTime(2017, 11, 05, 01, 00, 00), 3.2),
                    new Point(new DateTime(2017, 11, 05, 02, 00, 00), 7),
                    new Point(new DateTime(2017, 11, 05, 03, 00, 00), 8),
                    new Point(new DateTime(2017, 11, 06, 01, 00, 00), 9),
                }
            },
        };

        [Test]
        public void Test0()
        {
            DoTest(Data0, "Result0.txt");
        }

        [Test]
        public void Test1()
        {
            DoTest(Data1, "Result1.txt");
        }

        [Test]
        public void Test2()
        {
            DoTest(Data2, "Result2.txt");
        }

        [Test]
        public void Test3()
        {
            DoTest(Data3, "Result3.txt");
        }

        [Test]
        public void Test4()
        {
            DoTest(Data4, "Result4.txt", true);
        }

        [Test]
        public void Test5()
        {
            DoTest(Data5, "Result5.txt");
        }

        [Test]
        public void Test6()
        {
            DoTest(Data6, "Result6.txt");
        }

        [Test]
        public void Test7()
        {
            DoTest(Data7, "Result7.txt");
        }

        [Test]
        public void Test8()
        {
            DoTest(Data8, "Result8.txt");
        }

        [Test]
        public void Test9()
        {
            DoTest(Data9, "Result9.txt");
        }

        private static void DoTest(List<Series> data, string resultFile, bool reverse = false)
        {
            var watch = Stopwatch.StartNew();
            var res = SeriesMergeAlgo.Merge(data, reverse);
            watch.Stop();
            Console.WriteLine($"{watch.Elapsed.TotalSeconds:0.000000}");
            Console.WriteLine("Expected " + Const.Delimiter0);
            var expected = ReadResult(resultFile);
            Console.WriteLine(expected);
            Console.WriteLine("Actual " + Const.Delimiter0);
            var dump = DumpData(res);
            Console.WriteLine(dump);
            Assert.AreEqual(expected, dump);
        }

        static string ReadResult(string name)
        {
            var res = File.ReadAllText(BasePath + @"\" + name);
            return res;
        }

        static string DumpData(List<object[]> data)
        {
            var res = new StringBuilder();

            foreach (var line in data)
            {
                var text = string.Join("\t\t", line.Select(Format));
                res.AppendLine(text);
            }

            return res.ToString();
        }

        static string Format(object obj)
        {
            if (obj is DateTime date)
                return date.ToSortableString();
            return obj?.ToString();
        }
    }
}
