## Dependencies

SJTS is required. It expected to be in ../sjts directory. So the directory structure should look like:


    .
      ./sjts
      ./csharp-client


To clone SJTS sources:


    git clone https://bitbucket.org/shooju/sjts.git


## Development

To pack for nuget, use:
    
    cd Shooju
    nuget pack Shooju.csproj -Prop Configuration=Release

Before running the tests, add the following environment variables: 

    ShoojuTests.Server
    ShoojuTests.User
    ShoojuTests.ApiSecret

To run the tests, use either

    Resharper's test runner in Visual Studio - choose the menu "ReSharper / Unit Tests / Run All Tests From Solution"
    Run "packages\NUnit.ConsoleRunner.3.4.1\tools\nunit3-console.exe Shooju.Tests\bin\Debug\Shooju.Tests.dll" from the project's root
