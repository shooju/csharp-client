﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooju.Example
{
    public static class Util
    {
        public static string ToIsoString(this DateTime val)
        {
            return val.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        public static string ToSortableString(this DateTime val)
        {
            return val.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static bool SequenceEqual(this Dictionary<string, dynamic> vals1, Dictionary<string, dynamic> vals2)
        {
            if (vals1.Count != vals2.Count)
                return false;

            foreach (var tuple1 in vals1)
            {
                if (!vals2.TryGetValue(tuple1.Key, out var val2))
                    return false;
                var val1 = tuple1.Value;

                if (val1 is string textVal1)
                {
                    if (textVal1 != (string)val2)
                        return false;
                }
                else if (val1 is IConvertible num)
                {
                    if (!num.ToType(val2.GetType(), CultureInfo.InvariantCulture).Equals(val2))
                        return false;
                }
                else
                    throw new NotSupportedException();
            }

            return true;
        }

        public static void RefreshIndex(this Connection conn)
        {
            conn.RawPost("/series/refresh");
        }
    }
}
