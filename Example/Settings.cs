﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shooju.Example
{
    public static class Settings
    {
        public static string Server => Get("ShoojuTests.Server");
        public static string User => Get("ShoojuTests.User");
        public static string ApiSecret => Get("ShoojuTests.ApiSecret");

        static string Get(string name)
        {
            var res = Environment.GetEnvironmentVariable(name);
            return res;
        }
    }
}
