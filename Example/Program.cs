﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Linq;

using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Shooju.Exceptions;
using Shooju.Model;
using Shooju.Utils;

namespace Shooju.Example
{
    public class Program
    {
        static void Main()
        {
            var conn = new Connection(Settings.Server, Settings.User, Settings.ApiSecret);
            var prefix = $"users\\{Settings.User}\\CSharpClientSample\\{DateTime.UtcNow:yyyy-MM-dd-HH_mm_ss}";

            var job = conn.RegisterJob("csharp-test", batchSize: 2, maxPointsPerBatch: 1);

            //CleanupAll(conn);
            Cleanup(conn, prefix);

            RunRawAccess(conn);

            TestAccountConfig(conn);

            var fileId = TestUploadFile(conn, out _);
            TestDownloadFile(conn, fileId);

            var fileIdMulti = TestUploadFileMultiPart(conn, out _);
            TestDownloadFile(conn, fileIdMulti);

            TestDownloadNonExistingFile(conn);

            RunRemoteJobOperations(conn, job, prefix);
            TestSeriesDeletion(conn, job, prefix);
            TestSeriesDataReplacing(conn, job, prefix);

            RunWritePointsAndFields(conn, job, prefix);
            RunWriteReportedPointsAndFields(conn, job, prefix);

            RunQueryScrolling(conn, prefix);
            RunGettingSingleField(conn, prefix);
            RunGettingMultipleFields(conn, prefix);
            RunGettingPoints(conn, prefix);
            RunMultiGet(conn, job, prefix);

            RunGetSeriesPostApi(conn, job, prefix);

            RunGetFacets(conn, job, prefix);
            RunReportedDates(conn, job, prefix);

            Cleanup(conn, prefix);

            Console.WriteLine("To exit, press any key...");
            Console.ReadKey();
        }

        public static void RunReportedDates(Connection conn, RemoteJob job, string commonPrefix)
        {
            var prefix = $"{commonPrefix}\\{nameof(RunMultiGet)}";
            var seriesQuery1 = $"sid=\"{prefix}\\1\"";
            var seriesQuery2 = $"sid=\"{prefix}\\2\"";

            var reportedDt1 = new DateTime(2000, 1, 1);
            var reportedDt2 = new DateTime(2001, 1, 1);

            // write series with regular points
            var pointsData = new List<Point>
            {
                new Point(new DateTime(2015, 1, 1), 1000),
                new Point(new DateTime(2015, 2, 1), 2000),
                new Point(new DateTime(2015, 3, 1), 3000)
            };
            job.Write(seriesQuery1, points: pointsData);
            pointsData = new List<Point>
            {
                new Point(new DateTime(2016, 1, 1), 100),
                new Point(new DateTime(2016, 2, 1), 200),
                new Point(new DateTime(2016, 3, 1), 300)
            };
            job.Write(seriesQuery2, points: pointsData);

            // write series with reported points
            pointsData = new List<Point>
            {
                new Point(new DateTime(2017, 1, 1), -1000),
                new Point(new DateTime(2017, 2, 1), -2000),
                new Point(new DateTime(2017, 3, 1), -3000),
                new Point(new DateTime(2017, 4, 1), -5000)
            };
            job.WriteReported(seriesQuery1, points: pointsData, reportedDate: reportedDt1);

            pointsData = new List<Point>
            {
                new Point(new DateTime(2018, 1, 1), -10),
                new Point(new DateTime(2018, 2, 1), -20),
                new Point(new DateTime(2018, 3, 1), -30),
                new Point(new DateTime(2018, 4, 1), -50)
            };
            job.WriteReported(seriesQuery1, points: pointsData, reportedDate: reportedDt2);
            job.WriteReported(seriesQuery2, points: pointsData, reportedDate: reportedDt2);
            job.Submit();
            conn.RefreshIndex();

            // make sure regular points are written
            var ser = conn.GetSeries(seriesQuery1, maxPoints: 100);
            Assert.AreEqual(ser.Points.Count, 3);
            Assert.AreEqual(ser.Points[0].Date, new DateTime(2015, 1, 1));
            Assert.AreEqual(ser.Points[0].Value, 1000);

            // another series 
            ser = conn.GetSeries(seriesQuery2, maxPoints: 100);
            Assert.AreEqual(ser.Points.Count, 3);
            Assert.AreEqual(ser.Points[0].Date, new DateTime(2016, 1, 1));
            Assert.AreEqual(ser.Points[0].Value, 100);

            // then try read reported dates for same series
            ser = conn.GetSeries($"{seriesQuery1}@reppdate:{reportedDt1.ToIsoString()}", maxPoints: 100);
            Assert.AreEqual(ser.Points.Count, 4);
            Assert.AreEqual(ser.Points[0].Date, new DateTime(2017, 1, 1));
            Assert.AreEqual(ser.Points[0].Value, -1000);

            ser = conn.GetSeries($"{seriesQuery1}@reppdate:{reportedDt2.ToIsoString()}", maxPoints: 100);
            Assert.AreEqual(ser.Points.Count, 4);
            Assert.AreEqual(ser.Points[0].Date, new DateTime(2018, 1, 1));
            Assert.AreEqual(ser.Points[0].Value, -10);

            ser = conn.GetSeries($"{seriesQuery2}@reppdate:{reportedDt2.ToIsoString()}", maxPoints: 100);
            Assert.AreEqual(ser.Points.Count, 4);
            Assert.AreEqual(ser.Points[0].Date, new DateTime(2018, 1, 1));
            Assert.AreEqual(ser.Points[0].Value, -10);

            ser = conn.GetSeries($"{seriesQuery2}@reppdate:{reportedDt1.ToIsoString()}", maxPoints: 100);
            Assert.AreEqual(ser.Points, null);

            // test reported dates retrieving 
            var reportedDates = conn.GetReportedDates(seriesQuery1);
            Assert.AreEqual(reportedDates.Count, 2);
            Assert.AreEqual(reportedDates, new[] { reportedDt1, reportedDt2 }.ToList());

            reportedDates = conn.GetReportedDates(seriesQuery2);
            Assert.AreEqual(reportedDates.Count, 1);
            Assert.AreEqual(reportedDates, new[] { reportedDt2 }.ToList());

            reportedDates = conn.GetReportedDates(jobId: job.JobId);
            Assert.AreEqual(reportedDates.Count, 2);
            Assert.AreEqual(reportedDates, new[] { reportedDt1, reportedDt2 }.ToList());

            reportedDates = conn.GetReportedDates(jobId: job.JobId, df: new DateTime(2000, 2, 1), dt: new DateTime(2002, 2, 1));
            Assert.AreEqual(reportedDates, new[] { reportedDt2 }.ToList());

            reportedDates = conn.GetReportedDates(jobId: job.JobId, df: new DateTime(1999, 12, 1), dt: new DateTime(2000, 2, 1));
            Assert.AreEqual(reportedDates, new[] { reportedDt1 }.ToList());
        }

        public static void RunMultiGet(Connection conn, RemoteJob job, string commonPrefix)
        {
            var prefix = commonPrefix + $"\\{nameof(RunMultiGet)}";
            RunRemoteJobOperations(conn, job, prefix);

            Console.WriteLine("Multi get example:\n");
            var seriesId = $"{prefix}\\A";
            string seriesId2 = $"{prefix}\\B";

            var fieldList = new List<string> { "fielda", "fields_text", "invalid" };

            var mGet = conn.GetMultiGet();
            mGet.GetSeries($"sid=\"{seriesId}\"", df: new DateTime(2015, 1, 1), maxPoints: 1);
            mGet.GetSeries($"sid=\"{seriesId}\"", fields: new[] { "*" });
            mGet.GetSeries($"sid=\"{seriesId2}\"", fields: fieldList);
            mGet.GetSeries($"sid=\"{seriesId2}\"", fields: new[] { "fielda" });
            mGet.GetSeries($"sid=\"{seriesId2}\"", maxPoints: 30);

            // next requests are invalid series id
            mGet.GetSeries("sid=invalidId");
            mGet.GetSeries("sid=invalidId", df: new DateTime(2010, 1, 1));
            mGet.GetSeries("sid=invalidIdFields");

            // Since C# is a strongly typed language, the results had to be
            // normalized to avoid reflection or other patterns
            // so Fetch() returns a Series object that has SeriesId, Fields
            // and Points attributes, only attribute will have a value depending on
            // the type request, unless the request was not succesful so both Points
            // and Fields will be null
            var seriesList = mGet.Fetch();

            foreach (var series in seriesList)
            {
                Console.WriteLine(series.SeriesId ?? series.Error);

                if (series.Points != null)
                {
                    foreach (var pt in series.Points)
                    {
                        Console.WriteLine($"{pt.Date.ToString("s")} - {pt.Value}");
                    }
                }
                else if (series.Fields != null)
                {
                    foreach (var entry in series.Fields)
                    {
                        Console.WriteLine($"{entry.Key}: {entry.Value}");
                    }
                }
                Console.WriteLine();
            }

            Assert.That(seriesList.Count == 8, "FAIL: unexpected number of responses");
            Assert.That(seriesList[0].SeriesId == $"$sid=\"{prefix}\\A\"", "FAIL: unexpected series id");
            Assert.That(seriesList[0].Points.Count == 1, "FAIL: unexpected points count");
            Assert.That(seriesList[0].Points[0].Date == new DateTime(2015, 1, 1), "FAIL: unexpected points date");
            Assert.That(seriesList[0].Points[0].Value == 1000, "FAIL: unexpected points value");
            Assert.That(seriesList[0].Fields.Count == 0, "FAIL: unexpected fields count");

            Assert.That(seriesList[1].Fields.Count == 3, "FAIL: unexpected fields count");
            Assert.That(seriesList[1].Fields["fielda"].ToString() == "val0", "FAIL: unexpected field value");

            Assert.That(seriesList[2].Error == "series_not_found", "FAIL: did not get error");

            mGet.GetSeries($"sid=\"{seriesId}\"", fields: new[] { "*" });
            seriesList = mGet.Fetch();

            foreach (var series in seriesList)
            {
                Console.WriteLine(series.SeriesId);
                if (series.Points != null)
                {
                    foreach (var pt in series.Points)
                    {
                        Console.WriteLine($"{pt.Date.ToString("s")} - {pt.Value}");
                    }
                }
                else if (series.Fields != null)
                {
                    foreach (var entry in series.Fields)
                    {
                        Console.WriteLine($"{entry.Key}: {entry.Value}");
                    }
                }
                Console.WriteLine();
            }

            Assert.That(seriesList.Count == 1, "FAIL: unexpected number of responses");
            Assert.That(seriesList[0].SeriesId == $"$sid=\"{prefix}\\A\"", "FAIL: unexpected series id");
            Assert.That(seriesList[0].Points == null, "FAIL: unexpected points count");
            Assert.That(seriesList[0].Fields.Count == 3, "FAIL: unexpected fields count");
            Assert.That(seriesList[0].Fields["fielda"].ToString() == "val0", "FAIL: unexpected field value");
        }

        private static void RunGettingPoints(Connection conn, string prefix)
        {
            var seriesId = $"{prefix}\\A";

            var pts = conn.GetPoints("sdfsdfsdfasd", null, null);
            Assert.That(pts == null, "FAIL: unexpected points count");

            var points = conn.GetPoints(seriesId, new DateTime(2010, 1, 1), new DateTime(2015, 5, 1), 10);
            if (points != null)
                foreach (var point in points)
                {
                    Console.WriteLine($"{point.Date.ToString("s")} - {point.Value}");
                }
            Assert.That(points.Count == 3, "FAIL: unexpected points count");
            Assert.That(points[0].Date == new DateTime(2015, 1, 1), "FAIL: unexpected point");
            Assert.That(points[0].Value == 1000, "FAIL: unexpected point value");
            Assert.That(points[1].Date == new DateTime(2015, 2, 1), "FAIL: unexpected point");
            Assert.That(points[1].Value == 2000, "FAIL: unexpected point value");
            Assert.That(points[2].Date == new DateTime(2015, 3, 1), "FAIL: unexpected point");
            Assert.That(points[2].Value == 3000, "FAIL: unexpected point value");

            Console.WriteLine();

            // Points from MIN to MAX
            points = conn.GetPoints(seriesId, null, null, 10);
            if (points != null)
                foreach (var point in points)
                {
                    Console.WriteLine($"{point.Date.ToString("s")} - {point.Value}");
                }
            Assert.That(points[0].Date == new DateTime(2015, 1, 1), "FAIL: unexpected point");
            Assert.That(points[0].Value == 1000, "FAIL: unexpected point value");
            Assert.That(points[1].Date == new DateTime(2015, 2, 1), "FAIL: unexpected point");
            Assert.That(points[1].Value == 2000, "FAIL: unexpected point value");
            Assert.That(points[2].Date == new DateTime(2015, 3, 1), "FAIL: unexpected point");
            Assert.That(points[2].Value == 3000, "FAIL: unexpected point value");

            var point2 = conn.GetPoint(seriesId, new DateTime(2015, 1, 1));
            Console.WriteLine($"{point2.Date.ToString("s")} - {point2.Value}");
            Console.WriteLine();
            Assert.That(point2.Date == new DateTime(2015, 1, 1), "FAIL: unexpected point");
            Assert.That(point2.Value == 1000, "FAIL: unexpected point value");

            // Getting a point that should return a null value
            var point3 = conn.GetPoint(seriesId, new DateTime(1900, 1, 1));

            Console.WriteLine();
            Assert.That(point3 == null, "FAIL: unexpected point value");
        }

        private static void RunRawAccess(Connection conn)
        {
            // simple ping
            var res = conn.RawGet("/ping");
            Assert.That(res.GetValue("success").ToObject<Boolean>(), "FAIL: success is not true");
            Console.WriteLine("rawGet ping result: " + res);

            // set password
            var obj = new JObject
            {
                {"old_password", "mypass"},
                {"new_password", "mynewpass"}
            };
            res = conn.RawPost("/users/aldo/set_password/", null, obj);
            Console.WriteLine("rawPost set_password result: " + res);
            Assert.That(!res.GetValue("success").ToObject<Boolean>(), "FAIL: success is not false");
            Assert.That(res.GetValue("error").ToString() == "not_allowed", "FAIL: error != \"not_allowed\" ");
        }

        private static void RunGettingSingleField(Connection conn, string prefix)
        {
            var seriesId = $"{prefix}\\A";

            object field = conn.GetField(seriesId, "fielda").ToString();
            Assert.That(field.ToString() == "val0", "FAIL: unexpected field value");

            field = conn.GetField("sdfsdfsdfasd", "xxx");
            Assert.That(field == null, "FAIL: unexpected field value");

            field = conn.GetFields("sdfsdfsdfasd");
            Assert.That(field == null, "FAIL: unexpected field value");
        }

        public static void RunQueryScrolling(Connection conn, string prefix)
        {
            var fieldList = new List<string>
            {
                "fielda",
                "invalid"
            };
            var numOfSeries = 0;
            var checkedValues = false;
            Console.WriteLine("Scrolling through a query");

            var query = $"sid:\"{prefix}\"";
            var seriesList =
                conn.Scroll(query, fieldList, new DateTime(2015, 1, 1), new DateTime(2015, 4, 1), 5, extraParams: new Dictionary<string, object> { { "include_job", "y" } }).ToArray();
            foreach (var series in seriesList)
            {
                numOfSeries++;
                Console.WriteLine(series.SeriesId);
                foreach (var entry in series.Fields)
                {
                    Console.WriteLine($"{entry.Key}: {entry.Value}");
                }

                if (series.SeriesId == $"{prefix}\\A")
                {
                    Assert.That(series.Fields["fielda"].ToString() == "val0", "FAIL: unexpected field value");
                    Assert.That(series.Points[0].Date == new DateTime(2015, 1, 1), "FAIL: unexpected point");
                    Assert.That(series.Points[0].Value == 1000, "FAIL: unexpected point value");
                    Assert.IsNotNull(series.Points[0].JobId, "FAIL: unexpected point job value");
                    Assert.That(series.Points.Count == 3, "FAIL: unexpected points count");
                    checkedValues = true;
                }
            }
            Assert.That(checkedValues, "FAIL: did not meet series on scroll");
            Assert.That(numOfSeries == 5, $"FAIL: unexpected number of series {numOfSeries} instead of 5");

            Console.WriteLine($"{numOfSeries} series retrieved\n");

            numOfSeries = 0;
            Console.WriteLine("Scrolling through query, only retrieving series id");

            var seriesList2 = conn.Scroll(query).ToArray();
            foreach (var series in seriesList2)
            {
                numOfSeries++;
                Assert.That(!series.Fields.ContainsKey("fielda"), "FAIL: unexpectedly got field");
                Assert.That(series.Points == null, "FAIL: unexpected points set");
            }

            Console.WriteLine($"{numOfSeries} series retrieved\n");
        }

        public static void RunGettingMultipleFields(Connection conn, string prefix)
        {
            var seriesId = $"{prefix}\\A";
            var fieldList = new List<string> { "fielda", "fields_text", "invalid" };

            // this field won't be present in the Dictionary
            var fields = conn.GetFields(seriesId, fieldList);
            if (fields != null)
            {
                foreach (var each in fields)
                {
                    Console.WriteLine($"{each.Key}: {each.Value}");
                }
            }

            Console.WriteLine();
            Assert.That(fields.Count == 2, "FAIL: Unexpected field count");
            Assert.That(fields["fielda"].ToString() == "val0", "FAIL: Unexpected field value");
            Assert.That(fields["fields_text"].ToString() == "this is text", "FAIL: Unexpected field value");

            // GetFields with only series id returns all fields
            var allFields = conn.GetFields(seriesId);
            if (allFields != null)
            {
                foreach (var each in allFields)
                {
                    Console.WriteLine($"{each.Key}: {each.Value}");
                }
            }
            Console.WriteLine();
            Assert.That(allFields.Count == 3, "FAIL: Unexpected field count");
        }

        public static void InitVals1(Connection conn, RemoteJob job, string prefix)
        {
            var seriesQuery1 = $"sid={prefix}\\A";
            var seriesQuery2 = $"sid={prefix}\\B";
            var fieldsData = new Dictionary<string, dynamic>
            {
                {"fielda", "seriesId1"},
                {"fieldb", "a"},
                {"fieldc_num", 1000}
            };

            var pointsData = new List<Point>
            {
                new Point(new DateTime(2015, 1, 1), 1000),
                new Point(new DateTime(2015, 2, 1), 2000),
                new Point(new DateTime(2015, 3, 1), 3000)
            };

            job.Write(seriesQuery1, fieldsData, pointsData);

            fieldsData = new Dictionary<string, dynamic>
            {
                {"fielda", "seriesId2"},
                {"fieldb", "a"},
                {"fieldc_num", 2000}
            };

            pointsData = new List<Point>
            {
                new Point(new DateTime(2015, 1, 1), 3000),
                new Point(new DateTime(2015, 2, 1), 4000),
                new Point(new DateTime(2015, 3, 1), 5000)
            };

            job.Write(seriesQuery2, fieldsData, pointsData);

            job.Submit();

            conn.RefreshIndex();
        }

        public static void RunGetFacets(Connection client, RemoteJob job, string prefix)
        {
            var testPrefix = $"{prefix}\\{nameof(RunGetFacets)}";
            InitVals1(client, job, testPrefix);

            var response = client.GetFacets("fielda", $"sid:\"{testPrefix}\"", 20);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Facets.Count);
            var data = response.Facets.First().Value;

            Assert.AreEqual(2, data.Terms.Count);

            Assert.AreEqual(data.Terms[0].Term, "seriesId1");
            Assert.AreEqual(data.Terms[1].Term, "seriesId2");
            Assert.AreEqual(data.Terms[0].Count, 1);
            Assert.AreEqual(data.Terms[1].Count, 1);

            response = client.GetFacets("fielda", $"sid:\"{testPrefix}\"", 1);
            Assert.AreEqual(1, response.Facets.Count);
            data = response.Facets.First().Value;

            Assert.IsTrue(response.Success);
            Assert.AreEqual(data.Terms[0].Term, "seriesId1");
            Assert.AreEqual(data.Terms[0].Count, 1);
        }

        public static void RunRemoteJobOperations(Connection conn, RemoteJob job, string prefix)
        {
            // make sure series doesn't exist
            var seriesQuery = $"sid={prefix}\\A";

            // try to clean up...series should not actually exists
            string[] seriesIds = { "A", "B", "C", "D", "E", "F" };

            foreach (var curId in seriesIds)
            {
                var sid = $"{prefix}\\{curId}";
                var fields = conn.GetFields(sid);
                if (fields != null)
                    job.Delete(sid);
            }
            job.Submit();

            // make sure series doesn't exist
            var field = conn.GetField(seriesQuery, "l1");
            Assert.IsNull(field);

            field = conn.GetField(seriesQuery, "field1");
            Assert.IsNull(field);

            // write some test data
            var fieldsData = new Dictionary<string, dynamic>
            {
                {"fielda", "val0"},
                {"fields_text", "this is text"},
                {"field_num", 1000}
            };

            job.Write(seriesQuery, fieldsData, SamplePointsData1);
            job.Write(seriesQuery, null, SamplePointsData2);
            try
            {
                job.Write($"sid={prefix}\\I Error", new Dictionary<string, dynamic> { { "fielda", "val" }, { "fieldb_num", "hello" } });
                job.Submit();

                Assert.That(false, "FAIL: Exception is not raised");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            job.Write($"sid={prefix}\\C", new Dictionary<string, dynamic> { { "fielda", "val1" }, { "fieldb_num", 11 } });
            job.Write($"sid={prefix}\\D", new Dictionary<string, dynamic> { { "fielda", "val2" }, { "fieldb_num", 12 } });
            job.Write($"sid={prefix}\\E", new Dictionary<string, dynamic> { { "fielda", "val3" }, { "fieldb_num", 13 } });
            job.Write($"sid={prefix}\\F", new Dictionary<string, dynamic> { { "fielda", "val4" }, { "fieldb_num", 14 } });
            job.Submit();

            Assert.That(job.JobId > 0, "FAIL: unexpected job id");

            conn.RefreshIndex();

            // make sure values were really written
            field = conn.GetField($"{prefix}\\C", "fielda").ToString();
            Assert.That(field.ToString() == "val1", "FAIL: unexpected field value");

            seriesQuery = $"{prefix}\\D";
            field = conn.GetField(seriesQuery, "fielda").ToString();
            Assert.That(field.ToString() == "val2", "FAIL: unexpected field value");

            job.Finish();
            Console.WriteLine(job.JobId);
        }

        public static void TestSeriesDataReplacing(Connection conn, RemoteJob job, string prefix)
        {
            var seriesId = $"{prefix}\\{nameof(TestSeriesDataReplacing)}";
            var seriesQuery = $"sid={seriesId}";

            // init
            {
                Cleanup(conn, seriesId);

                job.Write(seriesQuery, SampleFieldsData1, SamplePointsData1, RemovalSwitch.All);
                job.Submit();
                conn.RefreshIndex();

                var series = conn.GetSeries(seriesQuery, new[] { "*" }, maxPoints: 10);
                Assert.That(series.Fields != null && series.Fields.Count == 2);
                Assert.That(series.Points != null && series.Points.Count == 3);
            }

            // testing
            {
                job.Write(seriesQuery, SampleFieldsData2, null, RemovalSwitch.Fields);
                job.Submit();
                conn.RefreshIndex();

                {
                    var series = conn.GetSeries(seriesQuery, new[] { "*" }, maxPoints: 10);
                    Assert.That(series.Points != null && series.Points.Count == 3);

                    object field = conn.GetField(seriesId, "fieldc").ToString();
                    Assert.That(field.ToString() == "val1_new");

                    var field1 = conn.GetField(seriesId, "fielda");
                    Assert.IsNull(field1);
                }

                job.Write(seriesQuery, points: SamplePointsData2, removeOthers: RemovalSwitch.Points);
                job.Submit();
                conn.RefreshIndex();

                {
                    var points = conn.GetPoints(seriesId, DateTime.MinValue, DateTime.MaxValue);
                    Assert.That(points != null && points.Count == 1);

                    object field = conn.GetField(seriesId, "fieldc").ToString();
                    Assert.That(field.ToString() == "val1_new");

                    var field1 = conn.GetField(seriesId, "fielda");
                    Assert.IsNull(field1);
                }

                job.Write(seriesQuery, SampleFieldsData1, SamplePointsData1, RemovalSwitch.All);
                job.Submit();
                conn.RefreshIndex();

                {
                    var fields = conn.GetFields(seriesId);
                    Assert.That(fields != null && fields.Count == 2);

                    var points = conn.GetPoints(seriesId, DateTime.MinValue, DateTime.MaxValue);
                    Assert.That(points != null && points.Count == 3);
                }
            }

            Cleanup(conn, seriesId);
        }

        public static void TestSeriesDeletion(Connection conn, RemoteJob job, string prefix)
        {
            var testPrefix = $"{prefix}\\{nameof(TestSeriesDataReplacing)}\\Delete";

            var seriesIds = new List<string>();
            for (var i = 0; i < 5; i++)
            {
                seriesIds.Add($"{testPrefix}\\{i}");
            }

            foreach (var seriesId in seriesIds)
            {
                job.Write($"sid={seriesId}", SampleFieldsData1);
            }
            job.Submit();
            conn.RefreshIndex();

            var deleteSeriesId = seriesIds.Last();
            var deleted = job.DeleteByQuery($"sid={deleteSeriesId}");
            Assert.That(deleted == 1);

            conn.RefreshIndex();

            {
                var fields = conn.GetFields(deleteSeriesId);
                Assert.IsNull(fields);
            }

            seriesIds.RemoveAt(seriesIds.Count - 1);

            foreach (var seriesId in seriesIds)
            {
                var fields = conn.GetFields(seriesId);
                Assert.IsNotNull(fields);
            }
            var deleted1 = job.DeleteByQuery($"sid:\"{testPrefix}\"");
            Assert.That(deleted1 == 4);

            conn.RefreshIndex();

            foreach (var seriesId in seriesIds)
            {
                var fields = conn.GetFields(seriesId);
                Assert.IsNull(fields);
            }
        }

        public static void RunWritePointsAndFields(Connection conn, RemoteJob job, string prefix)
        {
            var testSeries = $@"{prefix}\{nameof(RunWritePointsAndFields)}";
            var writeRequest = $@"sid={testSeries}";

            job.Write(writeRequest, SampleFieldsData1, SamplePointsData1);

            job.Submit();

            var response = conn.GetSeries($@"sid={testSeries}", fields: new[] { "*" }, maxPoints: 10);
            Assert.IsNotNull(response);
            Assert.That(response.Success);
            Assert.AreEqual(3, response.Points.Count);
            Assert.AreEqual(2, response.Fields.Count);
        }

        // ReSharper disable InconsistentNaming
        public static void RunWriteReportedPointsAndFields(Connection conn, RemoteJob job, string prefix)
        {
            var testSeries = $@"{prefix}\{nameof(RunWriteReportedPointsAndFields)}";
            var writeRequest = $@"sid={testSeries}";

            var reportedDate1 = new DateTime(2000, 01, 01);
            var reportedDate2 = new DateTime(2000, 01, 02);
            job.WriteReported(writeRequest, reportedDate1, SampleFieldsData1, SamplePointsData1);
            job.WriteReported(writeRequest, reportedDate2, SampleFieldsData2, SamplePointsData2);

            job.Submit();

            var getRequest1 = $"sid={testSeries}@repfdate:{reportedDate1.ToIsoString()}@reppdate:{reportedDate1.ToIsoString()}";
            var response1 = conn.GetSeries(getRequest1, fields: new[] { "*" }, maxPoints: -1);
            Assert.IsNotNull(response1);
            Assert.That(response1.Success);
            Assert.AreEqual(3, response1.Points.Count);
            Assert.AreEqual(2, response1.Fields.Count);

            Assert.That(response1.Fields.SequenceEqual(SampleFieldsData1));
            Assert.That(response1.Points.SequenceEqual(SamplePointsData1));

            var getRequest2 = $"sid={testSeries}@repfdate:{reportedDate2.ToIsoString()}@reppdate:{reportedDate2.ToIsoString()}";
            var response2 = conn.GetSeries(getRequest2, fields: new[] { "*" }, maxPoints: -1);
            Assert.IsNotNull(response2);
            Assert.That(response2.Success);
            Assert.AreEqual(1, response2.Points.Count);
            Assert.AreEqual(1, response2.Fields.Count);

            Assert.That(response2.Fields.SequenceEqual(SampleFieldsData2));
            Assert.That(response2.Points.SequenceEqual(SamplePointsData2));

            var getRequest2_1 = $"sid={testSeries}@repfdate:{reportedDate2.ToIsoString()}@reppdate:{reportedDate2.ToIsoString()}";
            var response2_1 = conn.GetSeries(getRequest2_1, fields: new[] { "*" });
            Assert.IsNotNull(response2_1);
            Assert.That(response2_1.Success);
            Assert.That(response2_1.Points == null || response2_1.Points.Count == 0);
            Assert.AreEqual(1, response2_1.Fields.Count);

            var getRequest2_2 = $"sid={testSeries}@repfdate:{reportedDate2.ToIsoString()}@reppdate:{reportedDate2.ToIsoString()}";
            var response2_2 = conn.GetSeries(getRequest2_2, maxPoints: -1);
            Assert.IsNotNull(response2_2);
            Assert.That(response2_2.Success);
            Assert.AreEqual(1, response2_2.Points.Count);
            Assert.That(response2_2.Fields == null || response2_2.Fields.Count == 0);

            var reportedDates = conn.GetReportedDates(seriesQuery: writeRequest);
            Assert.That(reportedDates.Count == 2);
            Assert.That(reportedDates.First() == reportedDate1);
            Assert.That(reportedDates.Last() == reportedDate2);
        }

        public static void Cleanup(Connection conn, string prefix)
        {
            var job = conn.RegisterJob("csharp-test-cleanup", batchSize: 1, maxPointsPerBatch: 1);
            var deleted = job.DeleteSeries($"sid:\"{prefix}\"", false, true);
            job.Finish();
            conn.RefreshIndex();
        }

        public static void CleanupAll(Connection conn)
        {
            var prefix = $"Users\\{Settings.User}";
            Cleanup(conn, prefix);
        }

        public static void RunGetSeriesPostApi(Connection conn, RemoteJob job, string prefix)
        {
            var testPrefix = $"{prefix}\\{nameof(RunGetSeriesPostApi)}";
            InitVals1(conn, job, testPrefix);

            {
                var seriesOld = conn.GetSeriesList(new[] { $"{testPrefix}\\A" }, new[] { "*" }, "MIN", "MAX", 10).ToArray();

                var startTime = DateTime.UtcNow;
                Series series = null;
                while ((DateTime.UtcNow - startTime).TotalMinutes < 10)
                {
                    series = conn.GetSeries($"sid=\"{testPrefix}\\A\"", fields: new[] { "*" }, maxPoints: 10);
                    if (series.Success)
                        break;
                }

                Assert.IsTrue(series != null && series.Success);
                Assert.AreEqual(3, series.Points.Count);
                Assert.AreEqual(3, series.Fields.Count);

                Assert.AreEqual(seriesOld.Length, 1);
                Assert.That(series.Points.SequenceEqual(seriesOld[0].Points));
                Assert.That(series.Fields.SequenceEqual(seriesOld[0].Fields));

                // include things
                var extraParams = new Dictionary<string, object> { { "include_job", "y" } };
                series = conn.GetSeries($"sid=\"{testPrefix}\\A\"", fields: new[] { "*" }, maxPoints: 10, extraParams: extraParams);
                Assert.AreEqual(series.Points[0].JobId, job.JobId);
                Assert.IsNull(series.Points[0].Timestamp);
                extraParams = new Dictionary<string, object> { { "include_job", "y" }, { "include_timestamp", "y" } };
                series = conn.GetSeries($"sid=\"{testPrefix}\\A\"", fields: new[] { "*" }, maxPoints: 10, extraParams: extraParams);
                Assert.AreEqual(series.Points[0].JobId, job.JobId);
                Assert.Greater(series.Points[0].Timestamp, new DateTime(2000, 1, 1));
            }

            {
                var mGet = conn.GetMultiGet();
                mGet.GetSeries($"sid=\"{testPrefix}\\A\"", fields: new[] { "*" }, maxPoints: 10);
                mGet.GetSeries($"sid=\"{testPrefix}\\B\"", fields: new[] { "*" }, maxPoints: 10);

                var bulkSeries = mGet.Fetch();
                Assert.AreEqual(2, bulkSeries.Count);
                Assert.IsFalse(bulkSeries.Any(cur => !cur.Success));

                var item0 = bulkSeries[0];
                Assert.AreEqual(3, item0.Points.Count);
                Assert.AreEqual(3, item0.Fields.Count);

                var item1 = bulkSeries[1];
                Assert.AreEqual(3, item1.Points.Count);
                Assert.AreEqual(3, item1.Fields.Count);

                //include things multiget
                mGet = conn.GetMultiGet();
                mGet.GetSeries($"sid=\"{testPrefix}\\A\"", fields: new[] { "*" }, maxPoints: 10);
                mGet.GetSeries($"sid=\"{testPrefix}\\B\"", fields: new[] { "*" }, maxPoints: 10);
                bulkSeries = mGet.Fetch(new Dictionary<string, object> { { "include_job", "y" } });
                item0 = bulkSeries[0];
                Assert.AreEqual(item0.Points[0].JobId, job.JobId);
                Assert.IsNull(item0.Points[0].Timestamp);

                mGet = conn.GetMultiGet();
                mGet.GetSeries($"sid=\"{testPrefix}\\A\"", fields: new[] { "*" }, maxPoints: 10);
                mGet.GetSeries($"sid=\"{testPrefix}\\B\"", fields: new[] { "*" }, maxPoints: 10);
                bulkSeries = mGet.Fetch(new Dictionary<string, object> { { "include_job", "y" }, { "include_timestamp", "y" } });
                item1 = bulkSeries[1];
                Assert.AreEqual(item1.Points[0].JobId, job.JobId);
                Assert.Greater(item1.Points[0].Timestamp, new DateTime(2000, 1, 1));
            }
        }

        public static string TestDownloadFile(Connection con, string fileId)
        {
            var content = con.DownloadFile(fileId);
            var fileSize = content.Length;
            Console.WriteLine($"file size: {fileSize}");

            var filePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            using (var file = con.DownloadFile(fileId, filePath))
            {
                file.Seek(0, SeekOrigin.End);
                Console.WriteLine($"got file at {filePath}");
                Console.WriteLine($"file size {file.Position}");
            }

            return filePath;
        }

        public static void TestDownloadNonExistingFile(Connection con)
        {
            // try some unknown file
            try
            {
                con.DownloadFile("non_existing");
            }
            catch (ShoojuClientException e)
            {
                Console.WriteLine($"got error on unknown file downloading which is expected {e.Message}");
            }
        }

        public static string TestUploadFile(Connection connection, out string fileName)
        {
            fileName = Path.GetTempPath() + @"\ShoojuTest." + Path.GetRandomFileName();
            var data = CreateRandomData();

            using (var stream = new FileStream(fileName, FileMode.CreateNew, FileAccess.ReadWrite))
            {
                stream.Write(data, 0, data.Length);
                stream.Seek(0, SeekOrigin.Begin);

                var fileId = connection.UploadFile(stream, Path.GetFileName(fileName));
                Assert.IsFalse(fileId.IsEmpty());
                return fileId;
            }
        }

        public static string TestUploadFileMultiPart(Connection connection, out string fileName)
        {
            fileName = Path.GetTempPath() + @"\ShoojuTest." + Path.GetRandomFileName();
            var data = CreateRandomData(5);

            var uploader = connection.InitMultiPartUpload(Path.GetFileName(fileName));
            uploader.UploadPart(1, data);
            uploader.UploadPart(2, data);

            var fileId = uploader.Complete();
            Assert.IsFalse(fileId.IsEmpty());
            return fileId;
        }

        public static void TestAccountConfig(Connection conn)
        {
            var config = conn.GetAccountConfig();
            Assert.IsTrue(config.ExcelLoadDlls.Count > 0);
        }

        static byte[] CreateRandomData(int size = 1)
        {
            var res = new byte[size * 1024 * 1024];
            RandomGen.NextBytes(res);
            return res;
        }

        public static void InitSampleData(Connection conn, RemoteJob job, string prefix)
        {
            var query = $"sid={prefix}";
            InitSampleDataByQuery(conn, job, query);
        }

        public static void InitSampleDataByQuery(Connection conn, RemoteJob job, string query)
        {
            job.Write(query, SampleFieldsData1, SamplePointsData1);
            job.Write(query, SampleFieldsData2, SamplePointsData2);

            job.Submit();

            conn.RefreshIndex();
        }

        public static readonly List<Point> SamplePointsData1 = new List<Point>
        {
            new Point(new DateTime(2015, 1, 1), 1000),
            new Point(new DateTime(2015, 2, 1), 2000),
            new Point(new DateTime(2015, 3, 1), 3000)
        };

        public static readonly List<Point> SamplePointsData2 = new List<Point>
        {
            new Point(new DateTime(2016, 2, 3), 1000),
        };

        static readonly Dictionary<string, dynamic> SampleFieldsData1 = new Dictionary<string, dynamic>
        {
            { "fielda", "val1" },
            { "fieldb_num", 11 },
        };

        static readonly Dictionary<string, dynamic> SampleFieldsData2 = new Dictionary<string, dynamic>
        {
            { "fieldc", "val1_new" },
        };

        static readonly Random RandomGen = new Random();
    }
}
