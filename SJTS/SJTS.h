// SJTS.h

#pragma once

using namespace System;

namespace SJTS {

	public ref class Converter
	{
		array<System::Byte> ^_sjts;
		int _cnt;
	public:
		delegate int Parse(int offs, int size);
		delegate void SetPoint(Int64 date, double value);
		Converter(array<System::Byte> ^sjts):_sjts(sjts) {}
		void convert(Parse ^parse, SetPoint ^setPoint);
	};
}
