// This is the main DLL file.

#include "stdafx.h"

#include "SJTS.h"

#define JSUINT32 auto
#define JSINT32 auto
#define JSUINT64 auto
#define JSINT64 auto

#define GET_UINT32(offs) System::BitConverter::ToUInt32(_sjts, offs)
#define GET_INT32(offs) System::BitConverter::ToInt32(_sjts, offs)
#define GET_INT64(offs) System::BitConverter::ToInt64(_sjts, offs)
#define GET_DOUBLE(offs, offs2) System::BitConverter::ToDouble(_sjts, offs + offs2 * 8)
#define PARSE_JSON _cnt = parse(json_offs, json_size)
#define GET_COUNT(t) _cnt
#define SET_POINT setPoint(date, val)

void SJTS::Converter::convert(Parse ^parse, SetPoint ^setPoint)
{
	int json_size = System::Buffer::ByteLength(_sjts);
#define SJTS_TEMPLATE_ONLY
#include "..\..\sjts\sjtsdec.c"
}
