﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Shooju.Exceptions;
using Shooju.Model;
using Shooju.Utils;

namespace Shooju
{
    /// <summary>
    /// Main interface with Shooju.
    /// Methods throw Exceptions on errors.
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// Instantiates a Shooju Connection using user / api key credentials.
        /// </summary>
        /// <param name="server">Shooju server to use, should start with the protocol (http or https) and end in a slash</param>
        /// <param name="user">Shooju user</param>
        /// <param name="apiKey">Shooju API Key</param>
        /// <param name="extraArgs">additional query GET params to append to requests (will be removed)</param>
        public Connection(string server, string user, string apiKey, Dictionary<string, object> extraArgs = null)
        {
            _extraArgs = extraArgs ?? new();
            if (!_extraArgs.ContainsKey("location_type"))
                _extraArgs.Add("location_type", "dotnet");
            if (!_extraArgs.ContainsKey("v"))
            {
                var version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                if (version.EndsWith(".0"))
                    version = version.Substring(0, version.Length - 2);
                _extraArgs.Add("v", version);
            }
            _client = new Client(server, user, apiKey, _extraArgs);
        }

        /// <summary>
        /// Registers a Job to write data.
        /// </summary>
        /// <param name="description">human-readable description of the job; at least 4 characters</param>
        /// <param name="batchSize">maximum number of series to send per batch</param>
        /// <param name="maxPointsPerBatch">maximum number of points that will trigger a batch submit</param>
        /// <param name="source">identifies the source</param>
        /// <returns>RemoteJob instance created</returns>
        public RemoteJob RegisterJob(string description, int batchSize = 1, int maxPointsPerBatch = 500000, string source = null)
        {
            return new RemoteJob(this._client, description, source, batchSize, maxPointsPerBatch);
        }

        /// <summary>
        /// DEPRECATED.  Will be removed in next major version.  Use GetSeries(query) without the $-prefix if using series queries, or GetSeries("sid=\"sid\"") if using series id.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="dFrom">date from</param>
        /// <param name="dTo">date to</param>
        /// <param name="maxPoints">maximum number of points to retrieve</param>
        /// <returns>list of Points retrieved</returns>
        [Obsolete]
        public List<Point> GetPoints(string seriesId, object dFrom, object dTo, int maxPoints = 1000)
        {
            var queryResponse = _client.GetSeries(seriesId, dFrom, dTo, null, maxPoints);

            if (!queryResponse.CheckError())
                return null;

            if (queryResponse.Series.Count != 1)
                throw new ShoojuClientException(false);
            var response = queryResponse.Series.First();

            if (!response.CheckError())
                return null;

            return response.Points;
        }

        /// <summary>
        /// DEPRECATED.  Will be removed in next major version.  Use GetSeries(query) without the $-prefix if using series queries, or GetSeries("sid=\"sid\"") if using series id.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="date">date of the point to retrieve</param>
        /// <returns>single Point retrieved</returns>
        [Obsolete]
        public Point GetPoint(string seriesId, DateTime date)
        {
            var points = GetPoints(seriesId, date, date);
            if (points == null || !points.Any()) // no point for that date
                return null;

            return points[0];
        }

        /// <summary>
        /// DEPRECATED.  Will be removed in next major version.  Use GetSeries(query) without the $-prefix if using series queries, or GetSeries("sid=\"sid\"") if using series id.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="fields">list of fields of retrieve; leave null for all</param>
        /// <returns>dictionary with the requested fields if present</returns>
        [Obsolete]
        public Dictionary<string, object> GetFields(string seriesId, List<string> fields = null)
        {
            SeriesResponseContainer responseContainer;

            if (fields == null)
            {
                responseContainer = _client.GetSeries(seriesId, null, null, null, 0, true);
            }
            else if (!fields.Any()) // if no fields were requested we don't even contact the server
            {
                return new Dictionary<string, object>();
            }
            else
            {
                responseContainer = _client.GetSeries(seriesId, null, null, fields);
            }

            if (!responseContainer.CheckError())
                return null;

            if (responseContainer.Series.Count != 1)
                throw new ShoojuClientException(false);
            var response = responseContainer.Series.First();

            if (!response.CheckError())
                return null;

            return response.Fields;
        }

        /// <summary>
        /// Retrieves an enumerable to iterate through the series of a query.
        /// Frequently used with foreach. Yields one Series object per iteration which contains SeriesId, Fields
        /// and Points.
        /// </summary>
        /// <param name="query">query that returns 0+ series</param>
        /// <param name="fields">list of fields to retrieve; use * for all non-meta</param>
        /// <param name="df">date FROM for points; can be DateTime, 'MAX', 'MIN', or relative date format</param>
        /// <param name="dt">date TO for points; can be DateTime, 'MAX', 'MIN', or relative date format</param>
        /// <param name="maxPoints">number of points to retrieve per series; use -1 for all</param>
        /// <param name="operators">extra operators for the query using the @ notation</param>
        /// <param name="sort">sort in FIELD1 ASC/DESC,FIELD2 ASC/DESC,... format</param>
        /// <param name="maxSeries">maximum series to return; leave as null for no maximum</param>
        /// <param name="extraParams">>extra parameters; refer to API Documentationt</param>
        /// <param name="scrollBatchSize">number of series to retrieve in one batch</param>
        /// <returns>SeriesResponseEnumerable, a IEnumerable of Series</returns>
        public SeriesResponseEnumerable Scroll(string query, IList<string> fields = null, object df = null, object dt = null, int maxPoints = 0,
            string operators = "", string sort = null, int? maxSeries = null, Dictionary<string, object> extraParams = null, int scrollBatchSize = 500)
        {
            var request = new RequestScrollable
            {
                DateFrom = df,
                DateTo = dt,
                Query = query,
                Operators = operators,
                Fields = fields,
                MaxPoints = maxPoints,
                SortOn = sort ?? "",
                ScrollBatchSize = scrollBatchSize,
                ExtraParams = extraParams,
                MaxSeries = maxSeries,
            };

            return new SeriesResponseEnumerable(_client, request, maxSeries);
        }

        /// <summary>
        /// DEPRECATED.  Will be removed in next major version.  Use GetMultiGet() instead.
        /// </summary>
        /// <param name="seriesIds">list of shooju series ids</param>
        /// <param name="fields">list of fields to retrieve</param>
        /// <param name="dFrom">date from for points</param>
        /// <param name="dTo">date to for points</param>
        /// <param name="maxPoints">maximum number of points to retrieve per series (use 0 for none)</param>
        /// <returns>List of Series</returns>
        [Obsolete]
        public List<Series> GetSeriesList(IList<string> seriesIds, IList<string> fields, object dFrom, object dTo,
            int maxPoints = 0)
        {
            var request = new Request
            {
                DateFrom = dFrom,
                DateTo = dTo,
                Fields = fields,
                MaxPoints = maxPoints,
            };

            var response = _client.GetSeriesList(request, seriesIds);

            response.CheckError();

            return response.Series;
        }


        /// <summary>
        /// Retrieves reported dates for one of the following (use only one): seriesQuery, jobId, processor.  Dates returned can be limited via df/dt.
        /// </summary>
        /// <param name="seriesQuery">returns reported dates written for series returned by that seriesQuery;</param>
        /// <param name="jobId">returns reported dates written by that job</param>
        /// <param name="processor">return reported dates for series written by that processor</param>
        /// <param name="df">DateTime to start the dates array</param>
        /// <param name="dt">DateTime to finish the dates array</param>
        /// <param name="mode">request mode</param>
        /// <returns>List of DateTime</returns>
        public List<DateTime> GetReportedDates(string seriesQuery = null, int? jobId = null, string processor = null,
            DateTime? df = null, DateTime? dt = null, ReportedDateMode mode = ReportedDateMode.All)
        {
            var response = _client.ReportedDates(seriesQuery, jobId, processor, df, dt, mode);
            response.CheckError();
            return response.ReportedDates;
        }

        /// <summary>
        /// Retrieves fields and points for a single series by query.
        /// </summary>
        /// <param name="seriesQuery">query that returns 1 series</param>
        /// <param name="fields">list of fields to retrieve; use * for all non-meta</param>
        /// <param name="df">date FROM for points; can be DateTime, 'MAX', 'MIN', or relative date format</param>
        /// <param name="dt">date TO for points; can be DateTime, 'MAX', 'MIN', or relative date format</param>
        /// <param name="maxPoints">number of points to retrieve per series; use -1 for all</param>
        /// <param name="extraParams">extra parameters; refer to API Documentationt</param>
        /// <returns>a Series</returns>
        public Series GetSeries(string seriesQuery, IList<string> fields = null, object df = null, object dt = null, int maxPoints = 0,
            Dictionary<string, object> extraParams = null)
        {
            var request = new RequestPost
            {
                DateFrom = df,
                DateTo = dt,
                Query = seriesQuery,
                Fields = fields,
                MaxPoints = maxPoints,
                ExtraParams = extraParams,
            };
            var response = _client.GetSeries(extraParams, request);
            response.CheckError();
            var series = response.Series.First();
            series.CheckError();
            return series;
        }

        /// <summary>
        /// Returns an enumerable to load dates-values to analytic libraries like Deedle.
        /// </summary>
        /// <param name="query">shooju query</param>
        /// <param name="dFrom">date from for points</param>
        /// <param name="dTo">date to for points</param>
        /// <param name="maxPoints">maximum number of points to retrieve per series (use 0 for none)</param>
        /// <param name="keyField">key field</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<Tuple<DateTime, string, double>> LoadDatesValues(string query, object dFrom, object dTo,
            int maxPoints = 0, string keyField = "sid")
        {
            foreach (var series in Scroll(query, new List<string> { keyField }, dFrom, dTo, maxPoints))
            {
                var key = (keyField == "sid" ? series.SeriesId : series.Fields[keyField].ToString());

                if (series.Points != null)
                {
                    foreach (var point in series.Points)
                    {
                        if (point.Value != null)
                        {
                            yield return new Tuple<DateTime, string, double>(point.Date, key, point.Value.Value);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns an enumerable to load fields-values to analytic libraries like Deedle.
        /// </summary>
        /// <param name="query">shooju query</param>
        /// <param name="fields">fields to retrieve</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<Tuple<string, string, string>> LoadFieldsValues(string query, IList<string> fields)
        {
            foreach (var series in Scroll(query, fields: fields, maxPoints: 0))
            {
                foreach (var field in series.Fields)
                {
                    yield return new Tuple<string, string, string>(series.SeriesId, field.Key, field.Value.ToString());
                }
            }
        }

        /// <summary>
        /// DEPRECATED.  Will be removed in next major version.  Use GetSeries(query) without the $-prefix if using series queries, or GetSeries("sid=\"sid\"") if using series id.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="field">name of the field</param>
        /// <returns>value of the field or null if not part of the series or series does not exist</returns>
        [Obsolete]
        public object GetField(string seriesId, string field)
        {
            var fields = new List<string>
            {
                field
            };
            var fieldDict = GetFields(seriesId, fields);

            if (fieldDict == null)
            {
                return null;
            }

            object value;
            if (fieldDict.TryGetValue(field, out value))
            {
                return value;
            }
            return null;
        }

        /// <summary>
        /// Creates a MultiGet object to retrieve multiple GetSeries() in one request.
        /// </summary>
        /// <returns>a MultiGet instance</returns>
        public MultiGet GetMultiGet()
        {
            return new MultiGet(this._client);
        }

        /// <summary>
        /// Pings the api and returns true on success.
        /// </summary>
        /// <returns>ping success bool</returns>
        public bool Ping()
        {
            try
            {
                var response = _client.Ping();
                response.CheckError();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets account info
        /// </summary>
        /// <returns>account info</returns>
        public AccountConfig GetAccountConfig()
        {
            var response = _client.Get<AccountConfigResponse>($"{Client.API_BASE}/account/config");
            response.CheckError();
            return response.AccountConfig;
        }

        /// <summary>
        /// Retrieves the GET function result of an API call. Returns a JObject
        /// object.
        /// </summary>
        /// <param name="api_endpoint">the part after /api/1 – starting with /</param>
        /// <param name="url_params">dictionary of key->values that get encoded and passed in url as ?key=value&key=value – optional</param>
        /// <returns>task that returns a JObject with the result of API method</returns>
        public JObject RawGet(string api_endpoint, Dictionary<string, object> url_params = null)
        {
            return _client.RawRequest(api_endpoint, url_params, Client.RawRequestType.get);
        }

        /// <summary>
        /// Retrieves the DELETE function result of an API call. Returns a JObject
        /// object.
        /// </summary>
        /// <param name="api_endpoint">the part after /api/1 – starting with /</param>
        /// <param name="url_params">dictionary of key->values that get encoded and passed in url as ?key=value&key=value – optional</param>
        /// <returns>task that returns a JObject with the result of API method</returns>
        public JObject RawDelete(string api_endpoint, Dictionary<string, object> url_params = null)
        {
            return _client.RawRequest(api_endpoint, url_params, Client.RawRequestType.delete);
        }

        /// <summary>
        /// Retrieves the POST function result of an API call. Returns a JObject
        /// object.
        /// </summary>
        /// <param name="apiEndpoint">the part after /api/1 – starting with /</param>
        /// <param name="urlParams">dictionary of key->values that get encoded and passed in url as ?key=value&key=value – optional</param>
        /// <param name="postBody">JSON body – optional</param>
        /// <returns>task that returns a JObject with the result of API method</returns>
        public JObject RawPost(string apiEndpoint, Dictionary<string, object> urlParams = null, JObject postBody = null)
        {
            string postBodyStr = postBody?.ToString(Formatting.None) ?? "";
            return _client.RawRequest(apiEndpoint, urlParams, Client.RawRequestType.post, postBodyStr);
        }

        /// <summary>
        /// Retrieves the facets for a field, filtered by query.
        /// </summary>
        /// <param name="facet">field to use for facets</param>
        /// <param name="query">query to filter facets by</param>
        /// <param name="maxFacets">maximum number of facets to retrieve</param>
        /// <returns>FacetResponses</returns>
        public FacetResponse GetFacets(string facet, string query, int maxFacets = 50)
        {
            FacetResponse response = _client.GetFacets(facet, query, maxFacets);
            response.CheckError();
            return response;
        }

        /// <summary>
        /// Downloads a file and writes its content to a stream.
        /// </summary>
        /// <param name="fileId">Shooju file id</param>
        /// <param name="outStream">Stream to write file content</param>
        void DownloadFile(string fileId, Stream outStream)
        {
            var url = $"/api/1/files/{fileId}/download";
            _client.DownloadBinaryRequest(url, outStream);
        }

        /// <summary>
        /// Downloads a file and returns its content.
        /// </summary>
        /// <param name="fileId">Shooju file id</param>
        /// <returns>Array of bytes</returns>
        public byte[] DownloadFile(string fileId)
        {
            var stream = new MemoryStream();
            DownloadFile(fileId, stream);
            return stream.ToArray();
        }

        /// <summary>
        /// Downloads a file, stores it to a local filesystem and returns opened file object.
        /// </summary>
        /// <param name="fileId">Shooju file id</param>
        /// <param name="fileName">The local path to save downloaded file</param>
        /// <returns>Opened FileStream</returns>
        public FileStream DownloadFile(string fileId, string fileName)
        {
            var stream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Read, 1024 * 1024,
                FileOptions.RandomAccess);

            DownloadFile(fileId, stream);

            stream.Flush();
            stream.Seek(0, SeekOrigin.Begin);

            return stream;
        }

        /// <summary>
        /// Uploads a file object to Shooju.
        /// </summary>
        /// <param name="sessionId">uploader session id</param>
        /// <param name="file">file-like object to upload</param>
        /// <param name="fileName">uploaded file name</param>
        /// <returns>Returns the Shooju file id</returns>
        [Obsolete]
        public string UploadFile(string sessionId, Stream file, string fileName)
        {
            return _client.UploadBinaryRequest($"/api/1/uploader/session/{sessionId}/files/", file, fileName);
        }

        /// <summary>
        /// Uploads a file object to Shooju.
        /// </summary>
        /// <param name="file">file-like object to upload</param>
        /// <param name="fileName">uploaded file name</param>
        /// <returns>Returns the Shooju file id</returns>
        public string UploadFile(Stream file, string fileName)
        {
            var res = _client.UploadBinaryRequest($"/api/1/files/", file, fileName);
            return res;
        }

        /// <summary>
        /// Init multi-part upload of a file object to Shooju.
        /// </summary>
        /// <param name="fileName">uploaded file name</param>
        /// <returns>Returns the Shooju file id</returns>
        public MultiPartUploader InitMultiPartUpload(string fileName)
        {
            var request = new
            {
                filename = fileName,
            };
            var res = _client.Post<MultiPartUploadResponse>("/api/1/files/multipart/init", request);
            return new MultiPartUploader(_client, res.FileId, fileName);
        }

        /// <summary>
        /// Renews the session API token
        /// </summary>
        /// <returns></returns>
        public string RenewSession(string agent, double expiration)
        {
            var url = $"{Client.API_BASE}/users/{_client.User}/sessions/create";

            var request = new SessionRenewRequest
            {
                Agent = agent,
                Expiration = expiration,
            };

            var response = _client.Post<SessionRenewResponse>(url, request);
            response.CheckError();

            return response.ApiKey;
        }

        /// <summary>
        /// Deletes the API session
        /// </summary>
        /// <param name="agent"></param>
        public void DeleteSession(string agent)
        {
            var url = $"{Client.API_BASE}/users/{_client.User}/sessions/delete";

            var request = new SessionDeleteRequest
            {
                Agent = agent,
            };

            var response = _client.Post<Response>(url, request);
            response.CheckError();
        }

        readonly Client _client;

        readonly Dictionary<string, object> _extraArgs;
    }

    public enum ReportedDateMode
    {
        All,
        Points,
        Fields,
    }
}
