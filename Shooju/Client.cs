﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Shooju.Exceptions;
using Shooju.Model;
using Shooju.Utils;

namespace Shooju
{
    internal class Client
    {
        private string server;
        private string apiKey;
        public string User { get; }

        //TODO: read this endpoints from a file
        public const string API_BASE = "api/1";
        private string PING_ENDPOINT = string.Format("{0}/ping", API_BASE);
        private string JOBS_ENDPOINT = string.Format("{0}/jobs", API_BASE);

        private string SERIES_ENDPOINT_UNI = string.Format("{0}/series", API_BASE);
        private string SERIES_DELETE_ENDPOINT = string.Format("{0}/series/delete", API_BASE);
        public string SERIES_DELETE_REPORTED_ENDPOINT = $"{API_BASE}/series/reported_dates/delete";
        private string SERIES_WRITE_ENDPOINT = string.Format("{0}/series/write", API_BASE);

        private readonly HttpClient _client;
        private readonly Dictionary<string, object> _extraArgs;

        public Client(string server, string user, string apiKey, Dictionary<string, object> extraArgs = null)
        {
            this.server = server;
            this.apiKey = apiKey;
            User = user;

            _client = CreateClient(Timeout.InfiniteTimeSpan);

            _extraArgs = extraArgs;
        }

        public Response Ping()
        {
            var response = this.Get<Response>(this.PING_ENDPOINT);
            return response;
        }

        public SeriesResponseContainer GetSeries(Request request)
        {
            var query = new QueryStringArgs
            {
                ["date_format"] = "milli",
                ["df"] = request.DateFromSerialized,
                ["dt"] = request.DateToSerialized,
                ["max_points"] = request.MaxPoints.ToString(),
                ["fields"] = request.FieldsSerialized
            };

            if (!string.IsNullOrEmpty(request.SeriesId))
                query["series_id"] = request.SeriesId;

            if (!string.IsNullOrEmpty(request.Query))
                query["query"] = request.Query;

            if (!string.IsNullOrEmpty(request.Operators))
                query["operators"] = request.Operators;

            if (request.SortOn != null)
                query["sort"] = request.SortOnSerialized;

            AppendCommonExtraArgs(query);

            var scrollableRequest = request as RequestScrollable;
            if (scrollableRequest != null)
            {
                if (scrollableRequest.MaxSeries == null || scrollableRequest.MaxSeries > scrollableRequest.ScrollBatchSize)
                {
                    //we actually need to scroll
                    query["scroll"] = "y";
                    query["scroll_batch_size"] = scrollableRequest.ScrollBatchSize.ToString();
                    scrollableRequest.WasScrolled = true;
                }
                else
                {
                    //there is no need to scroll because we're asking for less series than we would get in one batch... so just paginate 1
                    query["per_page"] = scrollableRequest.MaxSeries.ToString();
                    scrollableRequest.WasScrolled = false;
                }
                AppendExtraArgs(query, scrollableRequest.ExtraParams);
            }

            var path = $"{SERIES_ENDPOINT_UNI}?{query}";

            var response = Get<SeriesResponseContainer>(path);
            return response;
        }

        public SeriesResponseContainer GetSeries(Dictionary<string, object> extraParams = null, params RequestPost[] requests)
        {
            var query = new QueryStringArgs
            {
                ["date_format"] = "milli"
            };

            AppendCommonExtraArgs(query);
            AppendExtraArgs(query, extraParams);

            var path = $"{SERIES_ENDPOINT_UNI}?{query}";
            var postData = new
            {
                series_queries = requests,
            };

            var response = Post<SeriesResponseContainer>(path, postData);
            return response;
        }

        public SeriesResponseContainer GetSeriesList(Request request, IList<string> seriesIds)
        {
            var query = new QueryStringArgs
            {
                ["date_format"] = "milli",
                ["df"] = request.DateFromSerialized,
                ["dt"] = request.DateToSerialized,
                ["max_points"] = request.MaxPoints.ToString(),
                ["fields"] = request.FieldsSerialized
            };

            AppendCommonExtraArgs(query);

            var path = $"{SERIES_ENDPOINT_UNI}?{query}";
            var postData = new { series_ids = seriesIds };

            var response = Post<SeriesResponseContainer>(path, postData);
            return response;
        }

        public SeriesResponseContainer GetSeries(string seriesId, object dFrom, object dTo,
            IList<string> fields = null, int maxPoints = 0, bool allFields = false)
        {
            var request = new Request
            {
                DateFrom = dFrom,
                DateTo = dTo,
                SeriesId = seriesId,
                Fields = fields,
                MaxPoints = maxPoints,
                AllFields = allFields,
            };

            return GetSeries(request);
        }

        public CreateJobResponse CreateJob(string description, string source = null)
        {
            if (source == null)
                source = "api";

            CreateJobRequest req = new CreateJobRequest();
            req.description = description;
            req.source = source;
            req.notes = "";
            CreateJobResponse response = Post<CreateJobResponse>(this.JOBS_ENDPOINT, req);
            return response;
        }

        public ReportedDatesResponse ReportedDates(string seriesQuery = null, int? jobId = null, string processor = null,
            DateTime? df = null, DateTime? dt = null, ReportedDateMode mode = ReportedDateMode.All)
        {
            var request = new QueryStringArgs();
            if (seriesQuery != null)
            {
                request.Add("series_id", $"${seriesQuery}");
            }
            if (jobId != null)
            {
                request.Add("job_id", jobId.ToString());
            }
            if (processor != null)
            {
                request.Add("processor", processor);
            }
            if (df != null)
            {
                request.Add("df", ConversionUtil.DateTimeToUnixTimestamp(df.Value).ToString());
            }
            if (dt != null)
            {
                request.Add("dt", ConversionUtil.DateTimeToUnixTimestamp(dt.Value).ToString());
            }

            request.Add("mode", mode.ToString().ToLowerInvariant());

            AppendCommonExtraArgs(request);

            var path = $"{SERIES_ENDPOINT_UNI}/reported_dates?{request}";
            var response = Get<ReportedDatesResponse>(path);
            return response;
        }

        public FacetResponse GetFacets(string facet, string filterBy, int size = 20)
        {
            // building path
            var request = new QueryStringArgs
            {
                ["query"] = filterBy,
                ["facets"] = facet,
                ["max_facet_values"] = size.ToString(),
                ["per_page"] = "0"
            };

            AppendCommonExtraArgs(request);

            var path = $"{SERIES_ENDPOINT_UNI}?{request}";
            var response = Get<FacetResponse>(path);
            return response;
        }

        public SeriesResponseContainer Scroll(string scrollId)
        {
            var query = new QueryStringArgs
            {
                ["scroll_id"] = scrollId,
            };
            AppendCommonExtraArgs(query);
            var path = $"{SERIES_ENDPOINT_UNI}?{query}";

            var response = Get<SeriesResponseContainer>(path);
            return response;
        }

        public SubmitResponseContainer BulkSubmit(IReadOnlyList<ISubmitRequest> requests, NameValueCollection extraArgs = null)
        {
            var queryParams = new QueryStringArgs();
            if (extraArgs != null)
            {
                foreach (string key in extraArgs)
                {
                    var val = extraArgs[key];
                    queryParams.Add(key, val);
                }
            }
            if (queryParams["date_format"] == null)
            {
                queryParams["date_format"] = "milli";
            }
            AppendCommonExtraArgs(queryParams);

            var path = $"{SERIES_WRITE_ENDPOINT}?{queryParams}";
            var data = new { series = requests.ToList() };

            var response = Post<SubmitResponseContainer>(path, data);
            response.CheckError();

            return response;
        }

        public int Delete(object requestBody)
        {
            var response = Delete<ResponseDeleted>(SERIES_DELETE_ENDPOINT, requestBody);
            response.CheckError();

            var res = response.Deleted;
            return res;
        }

        public enum RawRequestType
        {
            get,
            delete,
            post
        }

        public JObject RawRequest(string api_endpoint, Dictionary<string, object> url_params, RawRequestType type, string post_body = "")
        {
            // building path
            var query = new QueryStringArgs();
            if (url_params != null)
            {
                foreach (var prop in url_params)
                {
                    query[prop.Key] = prop.Value.ToString();
                }
            }

            AppendCommonExtraArgs(query);

            string path = $"{API_BASE}{api_endpoint}?{query}";

            var content = DoRequestRetry(
                () =>
                {
                    if (type == RawRequestType.delete)
                    {
                        var request = new HttpRequestMessage
                        {
                            Content = new StringContent(post_body, Encoding.UTF8, "application/json"),
                            Method = HttpMethod.Delete,
                            RequestUri = new Uri(_client.BaseAddress + path)
                        };
                        return _client.SendAsync(request);
                    }
                    else if (type == RawRequestType.post)
                    {
                        var requestContent = new StringContent(post_body, Encoding.UTF8, "application/json");
                        return _client.PostAsync(path, requestContent);
                    }
                    else if (type == RawRequestType.get)
                    {
                        return _client.GetAsync(path);
                    }
                    else
                        throw new NotSupportedException();
                },
                new[] { API_BASE, api_endpoint, type.ToString() });

            var responseText = content.ReadAsStringAsync().Result;
            return JObject.Parse(responseText);
        }

        public void DownloadBinaryRequest(string apiEndpoint, Stream outStream)
        {
            var filePos = outStream.Position;

            DoRequestRetry(
                () =>
                {
                    outStream.Seek(filePos, SeekOrigin.Begin);

                    var response = _client.GetAsync(apiEndpoint, HttpCompletionOption.ResponseHeadersRead);

                    var content = response.Result.Content;
                    Download(apiEndpoint, content, outStream);
                    return response;
                },
                new[] { API_BASE, apiEndpoint });
        }

        private static void Download(string apiEndpoint, HttpContent content, Stream outStream)
        {
            using (var downloadStream = content.ReadAsStreamAsync().Result)
            {
                var contentType = content.Headers.ContentType.MediaType;
                var errorType = "application/json";
                if (contentType == errorType)
                {
                    var errorText = Encoding.UTF8.GetString(downloadStream.ReadAllBytes());
                    var errorObj = JsonConvert.DeserializeObject<Response>(errorText);
                    var message = $"Downloading error: '{errorObj.Description ?? errorObj.Error}' -- '{apiEndpoint}'";
                    throw new ShoojuClientException(message);
                }

                var buf = new byte[16 * 1024];

                while (true)
                {
                    var bytesRead = downloadStream.Read(buf, 0, buf.Length);
                    if (bytesRead <= 0)
                        break;
                    outStream.Write(buf, 0, bytesRead);
                }
            }
        }

        public string UploadBinaryRequest(string apiEndpoint, Stream file, string fileName)
        {
            var boundary = new string('-', 4) + DateTime.Now.Ticks.ToString("x");
            var filePos = file.Position;

            var responseContent = DoRequestRetry(
                () =>
                {
                    file.Seek(filePos, SeekOrigin.Begin);
                    // prevent HttpClient from closing the source stream
                    var bufStream = new WrapperStream(file);

                    var fileContent = new StreamContent(bufStream);
                    fileContent.Headers.Remove("Content-Disposition");
                    fileContent.Headers.Add("Content-Disposition", $"form-data; name=\"file\"; filename=\"{fileName}\"");

                    var content = new MultipartContent("form-data", boundary)
                    {
                        fileContent
                    };

                    return _client.PostAsync(apiEndpoint, content);
                },
                new[] { API_BASE, apiEndpoint });

            var responseText = responseContent.ReadAsStringAsync().Result;
            var responseObj = JsonConvert.DeserializeObject<UploadResponse>(responseText);
            if (responseObj.Success)
            {
                return responseObj.FileId?.FirstOrDefault();
            }
            else
            {
                var message = $"Uploading error: '{responseObj.Description ?? responseObj.Error}' -- '{apiEndpoint}'";
                throw new ShoojuClientException(message);
            }
        }

        private HttpClient CreateClient(TimeSpan timeout)
        {
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var res = new HttpClient(new TimeoutHandler())
            {
                Timeout = timeout,
                BaseAddress = new Uri(this.server)
            };

            var headers = res.DefaultRequestHeaders;

            // adding accept headers
            headers.Accept.Clear();
            headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // adding simple auth headers
            headers.Authorization = new AuthenticationHeaderValue("Bearer",
                Convert.ToBase64String(Encoding.ASCII.GetBytes($"{User}:{apiKey}")));

            return res;
        }

        private static HttpContent DoRequestRetry(Func<Task<HttpResponseMessage>> taskGetter, IReadOnlyCollection<object> requestInfo)
        {
            var delays = new[] { 4, 8, 15, 30 };
            HttpResponseMessage response = null;

            for (var i = 0; i <= delays.Length; i++)
            {
                try
                {
                    var task = Task.Run(async () =>
                    {
                        var httpTask = taskGetter();
                        return await httpTask;
                    });
                    response = task.Result;

                    if (response.IsSuccessStatusCode || RetryOnHttpCode.All(cur => cur != response.StatusCode))
                        break;
                }
                catch (Exception exc)
                {
                    if (!exc.IsConnectionError() || i >= delays.Length)
                    {
                        var message = $"Connection error:\r\n{FormatRequestInfo(requestInfo)}\r\n{Const.Delimiter2}\r\n{exc}";
                        throw new ShoojuClientException(message, exc, serverError: false);
                    }
                }

                if (i >= delays.Length)
                    break;
                Thread.Sleep(TimeSpan.FromSeconds(delays[i]));
            }

            if (response == null)
                throw new ShoojuClientException($"API access failed: \r\n{FormatRequestInfo(requestInfo)}", false);

            if (!response.IsSuccessStatusCode)
            {
                string responseText = null;
                try
                {
                    responseText = response.Content.ReadAsStringAsync().Result;
                }
                catch (Exception exc)
                {
                    Trace.WriteLine(exc);
                }

                var message = $"API access failed: {(int)response.StatusCode} ({response.ReasonPhrase})\r\n{FormatRequestInfo(requestInfo)}";
                if (!responseText.IsEmpty())
                {
                    var length = Math.Min(responseText.Length, 5_000);
                    message += $"\r\n{Const.Delimiter2}\r\n{responseText.Substring(0, length)}";
                }

                throw new ShoojuClientException(message, false);
            }

            return response.Content;
        }

        private static readonly HttpStatusCode[] RetryOnHttpCode =
        {
            HttpStatusCode.BadGateway,
            HttpStatusCode.ServiceUnavailable,
            HttpStatusCode.GatewayTimeout,
        };

        private T DoRequestRetry<T>(Func<Task<HttpResponseMessage>> taskGetter, IReadOnlyCollection<object> requestInfo)
            where T : Response
        {
            var content = DoRequestRetry(taskGetter, requestInfo);

            string responseText;
            if (typeof(T) == typeof(SeriesResponseContainer))
            {
                var responseBytes = content.ReadAsByteArrayAsync().Result;
                var converter = new SJTSConverter(responseBytes);
                try
                {
                    var res = (T)((Response)converter.convert());
                    return res;
                }
                catch (ShoojuClientException)
                {
                    responseText = Encoding.UTF8.GetString(responseBytes, 0, responseBytes.Length);
                }
            }
            else
            {
                responseText = content.ReadAsStringAsync().Result;
            }
            var obj = (T)JsonConvert.DeserializeObject(responseText, typeof(T), RequestBase.JsonSerializerSettings);
            return obj;
        }

        private static string FormatRequestInfo(IReadOnlyCollection<object> requestInfo)
        {
            var res = new StringBuilder();

            foreach (var cur in requestInfo)
            {
                var httpRequest = cur as HttpRequestMessage;
                if (httpRequest == null)
                {
                    res.AppendLine(cur.ToString());
                }
                else
                {
                    httpRequest.Headers.Authorization = null;
                    res.AppendLine(httpRequest.ToString());
                }
            }

            return res.ToString();
        }

        private T DoRequest<T>(string path, HttpContent content = null)
            where T : Response
        {
            var isPost = content != null;
            var method = isPost ? HttpMethod.Post : HttpMethod.Get;
            var requestInfo = new object[] { _client.BaseAddress, method, path };

            if (typeof(T) == typeof(SeriesResponseContainer))
            {
                var res = DoRequestRetry<T>(
                    () =>
                    {
                        var request = new HttpRequestMessage(method, path);
                        foreach (var header in _client.DefaultRequestHeaders)
                            request.Headers.TryAddWithoutValidation(header.Key, header.Value);
                        request.Headers.Add("Sj-Receive-Format", "sjts");
                        request.Content = content.Clone();

                        return _client.SendAsync(request);
                    },
                    requestInfo
                );
                return res;
            }
            else
            {
                if (isPost)
                {
                    var res = DoRequestRetry<T>(
                        () => _client.PostAsync(path, content.Clone()),
                        requestInfo);
                    return res;
                }
                else
                {
                    var res = DoRequestRetry<T>(
                        () => _client.GetAsync(path),
                        requestInfo);
                    return res;
                }
            }
        }

        public T Get<T>(string path)
            where T : Response
        {
            var res = DoRequest<T>(path);
            return res;
        }

        public T Post<T>(string path, object data)
            where T : Response
        {
            var request = JsonConvert.SerializeObject(data, RequestBase.JsonSerializerSettings);
            var content = new StringContent(request, Encoding.UTF8, "application/json");
            var res = DoRequest<T>(path, content);
            return res;
        }

        internal T Delete<T>(string path, object data)
            where T : Response
        {
            var requestBody = JsonConvert.SerializeObject(data, RequestBase.JsonSerializerSettings);
            var method = HttpMethod.Delete;
            var requestInfo = new object[] { _client.BaseAddress, method, path };

            return DoRequestRetry<T>(
                () =>
                {
                    var request = new HttpRequestMessage
                    {
                        Content = new StringContent(requestBody, Encoding.UTF8, "application/json"),
                        Method = method,
                        RequestUri = new Uri(_client.BaseAddress + path)
                    };

                    return _client.SendAsync(request);
                },
                requestInfo
            );
        }

        public Response FinishJob(int jobId)
        {
            Response response = Post<Response>(this.JOBS_ENDPOINT + string.Format("/{0}/finish", jobId), null);
            return response;
        }

        private void AppendCommonExtraArgs(QueryStringArgs query)
        {
            AppendExtraArgs(query, _extraArgs);
        }

        private void AppendExtraArgs(QueryStringArgs query, Dictionary<string, object> args)
        {
            if (args == null)
                return;

            foreach (var pair in args)
            {
                if (pair.Value != null)
                    query[pair.Key] = pair.Value.ToString();
                else
                    query.Remove(pair.Key);
            }
        }
    }
}
