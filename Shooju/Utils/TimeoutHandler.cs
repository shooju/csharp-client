﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shooju.Utils
{
    // NOTE see https://thomaslevesque.com/2018/02/25/better-timeout-handling-with-httpclient/
    class TimeoutHandler : DelegatingHandler
    {
        public TimeoutHandler()
        {
            InnerHandler = new HttpClientHandler();
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                return await base.SendAsync(request, cancellationToken);
            }
            catch (OperationCanceledException exc) when (!cancellationToken.IsCancellationRequested)
            {
                throw new TimeoutException("The request has timed out.", exc);
            }
        }
    }
}
