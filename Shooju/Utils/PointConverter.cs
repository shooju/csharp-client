﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Newtonsoft.Json;

using Shooju.Model;

namespace Shooju.Utils
{
    class PointConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            var res = typeof(Point).IsAssignableFrom(objectType);
            return res;
        }

        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.StartArray)
                return null;

            if (!reader.Read())
                return null;

            var timeStampNum = ParseNumber(reader);
            if (timeStampNum == null)
                return null;

            var dateTime = ConversionUtil.UnixTimeStampToDateTime(timeStampNum.Value);

            if (!reader.Read())
                return null;

            var val = ParseNumber(reader);
            if (val == null)
                return null;

            if (!reader.Read())
                return null;
            if (reader.TokenType != JsonToken.EndArray)
                return null;

            var res = new Point(dateTime, val.Value);
            return res;
        }

        static double? ParseNumber(JsonReader reader)
        {
            if (reader.TokenType == JsonToken.Integer)
            {
                return (long)reader.Value;
            }
            else
            {
                if (reader.TokenType == JsonToken.Float)
                    return (double)reader.Value;
                else
                    return null;
            }
        }
    }
}
