﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Shooju.Utils
{
    public static class ConversionUtil
    {
        /// <summary>
        /// Converts a DateTime to a UnixTimeStamp in millis
        /// </summary>
        /// <param name="dt">DateTime to be converted to millis</param>
        /// <returns>unix timestamp in milliseconds</returns>
        public static long DateTimeToUnixTimestamp(DateTime dt)
        {
            return (long)((dt - new DateTime(1970, 1, 1)).TotalMilliseconds);
        }

        /// <summary>
        /// Converts a unix timestamp in millis to a DateTime object
        /// </summary>
        /// <param name="unixTimeStamp">unix time stamp in millis</param>
        /// <returns>a DateTime object</returns>
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); // epoch
            dt = dt.AddMilliseconds(unixTimeStamp);
            return dt;
        }

        public static string DateToString(DateTime? val, string defaultVal)
        {
            if (!val.HasValue)
                return defaultVal;

            if (val.Value == DateTime.MinValue)
                return "MIN";

            if (val.Value == DateTime.MaxValue)
                return "MAX";

            return DateTimeToUnixTimestamp(val.Value).ToString(CultureInfo.InvariantCulture);
        }

        public static DateTime StringToDate(string val)
        {
            var millis = long.Parse(val);
            var res = UnixTimeStampToDateTime(millis);
            return res;
        }
    }
}
