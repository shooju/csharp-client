﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shooju.Utils
{
    public static class StringUtil
    {
        public static bool IsEmpty(this string val)
        {
            return string.IsNullOrEmpty(val);
        }

        public static void Append(this StringBuilder target, string val, string delimiter)
        {
            if (target.Length > 0)
                target.Append(delimiter);
            target.Append(val);
        }
    }
}
