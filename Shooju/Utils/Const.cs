﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shooju.Utils
{
    public static class Const
    {
        public static readonly string Delimiter0 = new string('=', 80);
        public static readonly string Delimiter00 = new string('=', 40);
        public static readonly string Delimiter1 = new string('=', 15);
        public static readonly string Delimiter2 = new string('-', 10);
    }
}
