﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shooju.Utils
{
    class QueryStringArgs
    {
        public void Add(string key, string val)
        {
            _vals.Add(key, val);
        }

        public void Remove(string key)
        {
            _vals.Remove(key);
        }

        public string this[string i]
        {
            get { return _vals[i]; }
            set { _vals[i] = value; }
        }

        public override string ToString()
        {
            var res = string.Join("&", _vals.Select(pair => $"{pair.Key}={Escape(pair.Value)}"));
            return res;
        }

        static string Escape(string val)
        {
            if (val.IsEmpty())
                return "";
            return Uri.EscapeDataString(val);
        }

        readonly Dictionary<string, string> _vals = new Dictionary<string, string>();
    }
}
