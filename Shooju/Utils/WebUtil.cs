﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Web;

namespace Shooju.Utils
{
    public static class WebUtil
    {
        public static StreamContent Clone(this HttpContent content)
        {
            if (content == null)
                return null;

            var stream = new MemoryStream();
            content.CopyToAsync(stream).Wait();
            stream.Position = 0;
            var res = new StreamContent(stream);
            return res;
        }

        public static bool IsConnectionError(this Exception exc)
        {
            var res = exc is HttpRequestException || exc is HttpListenerException ||
                      exc is WebException || exc is SocketException || exc is IOException;
            if (!res && exc is AggregateException aggExc)
            {
                res = aggExc.InnerExceptions.Any(IsConnectionError);
            }
            return res;
        }
    }
}
