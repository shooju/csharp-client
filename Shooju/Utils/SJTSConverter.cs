﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shooju.Exceptions;
using Shooju.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooju.Utils
{
    public class SJTSConverter
    {
        private byte[] _sjts;
        private bool _includes_job;
        private bool _includes_timestamp;
        private int _extraOffs;
        SJTS.Converter _conv;
        SJTSSeriesResponseContainer _obj;
        SJTSSeries _series;

        static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
        };

        private class SJTSSeriesResponseContainer : SeriesResponseContainer
        {
            [JsonProperty(PropertyName = "series")]
            public int SeriesLength { get; set; }
        }

        private class SJTSSeries : Series
        {
            [JsonProperty(PropertyName = "points")]
            public int PointsLength { get; set; }
        }

        private int parse(int offs, int size)
        {
            if (offs == 0) throw new ShoojuClientException("Invalid SJTS format!", false);
            int len;
            if (offs == 8)
            {
                int idx = Array.IndexOf(_sjts, (byte)0, offs, size);
                var json = System.Text.Encoding.UTF8.GetString(_sjts, offs, idx > 0 ? (idx - offs) : size);
                _extraOffs = idx + 1;
                _obj = (SJTSSeriesResponseContainer)JsonConvert.DeserializeObject(json, typeof(SJTSSeriesResponseContainer), JsonSerializerSettings);
                object sjts;
                if (_obj.RawResponse.TryGetValue("sjts", out sjts))
                {
                    var obj = (JObject)sjts;
                    JToken flag;
                    _includes_job = obj.TryGetValue("includes_job", out flag) && flag.ToString().CompareTo("True") == 0;
                    _includes_timestamp = obj.TryGetValue("includes_timestamp", out flag) && flag.ToString().CompareTo("True") == 0;
                }
                len = _obj.SeriesLength;
                if (len > 0) _obj.Series = new List<Series>(len);
            }
            else
            {
                var json = System.Text.Encoding.UTF8.GetString(_sjts, offs, size);
                _series = (SJTSSeries)JsonConvert.DeserializeObject(json, typeof(SJTSSeries), JsonSerializerSettings);
                len = _series.PointsLength;
                if (len > 0) _series.Points = new List<Point>(len);
                _obj.Series.Add(_series);
            }
            return len;
        }

        private void setPoint(Int64 date, double value)
        {
            var point = new Model.Point(ConversionUtil.UnixTimeStampToDateTime(date), value);
            if (_includes_job)
            {
                point.JobId = System.BitConverter.ToUInt32(_sjts, _extraOffs);
                _extraOffs += 4;
            }
            if (_includes_timestamp)
            {
                point.Timestamp = ConversionUtil.UnixTimeStampToDateTime(System.Convert.ToDouble(System.BitConverter.ToInt64(_sjts, _extraOffs)));
                _extraOffs += 8;
            }
            _series.Points.Add(point);
        }

        public SeriesResponseContainer convert()
        {
            _conv.convert(new SJTS.Converter.Parse(parse), new SJTS.Converter.SetPoint(setPoint));
            return (SeriesResponseContainer)_obj;
        }

        public SJTSConverter(byte[] sjts)
        {
            _conv = new SJTS.Converter(sjts);
            _sjts = sjts;
        }
    }
}
