﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Shooju.Utils
{
    public static class FileUtil
    {
        public static byte[] ReadAllBytes(this Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public static void Write(this Stream stream, string val, Encoding encoding)
        {
            var bytes = encoding.GetBytes(val);
            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
        }
    }
}
