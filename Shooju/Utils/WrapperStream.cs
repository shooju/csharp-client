﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Shooju.Utils
{
    class WrapperStream : Stream
    {
        public WrapperStream(Stream that)
        {
            _that = that;
        }

        public override void Flush()
        {
            _that.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _that.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _that.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _that.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _that.Write(buffer, offset, count);
        }

        public override bool CanRead => _that.CanRead;
        public override bool CanSeek => _that.CanSeek;
        public override bool CanWrite => _that.CanWrite;
        public override long Length => _that.Length;
        public override long Position
        {
            get => _that.Position;
            set => _that.Position = value;
        }

        private readonly Stream _that;
    }
}
