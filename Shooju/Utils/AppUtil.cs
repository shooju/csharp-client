﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Shooju.Utils
{
    public static class AppUtil
    {
        public static string GetCallingAssemblyPath()
        {
            var location = Assembly.GetCallingAssembly().CodeBase;
            location = (new Uri(location)).LocalPath;
            location = Path.GetDirectoryName(location);
            return location;
        }
    }
}
