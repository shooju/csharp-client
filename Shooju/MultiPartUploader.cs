﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Shooju.Model;

namespace Shooju
{
    public class MultiPartUploader
    {
        internal MultiPartUploader(Client client, string fileId, string fileName)
        {
            _fileId = fileId;
            _fileName = fileName;
            _client = client;
        }

        /// <summary>
        /// Upload file part
        /// </summary>
        /// <param name="partNum">unique file part number</param>
        /// <param name="data">data stream</param>
        public void UploadPart(int partNum, Stream data)
        {
            var uri = $"/api/1/files/multipart/upload?file_id={_fileId}&part_num={partNum}";
            _client.UploadBinaryRequest(uri, data, _fileName);
        }
        /// <summary>
        /// Upload file part
        /// </summary>
        /// <param name="partNum">unique file part number</param>
        /// <param name="data">bytes array</param>
        public void UploadPart(int partNum, byte[] data)
        {
            var stream = new MemoryStream(data);
            UploadPart(partNum, stream);
        }
        /// <summary>
        /// Complete file parts upload
        /// </summary>
        /// <returns>Returns the Shooju file id</returns>
        public string Complete()
        {
            _client.Post<Response>($"/api/1/files/multipart/complete?file_id={_fileId}", null);
            return _fileId;
        }

        readonly string _fileId;
        readonly string _fileName;
        readonly Client _client;
    }
}
