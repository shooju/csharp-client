﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

using Shooju.Model;

// ReSharper disable InconsistentNaming

namespace Shooju
{
    public class CreateJobResponse : Response
    {
        public int job_id { get; set; }
    }

    public class SearchResponse : Response
    {
        public BlockFields fields { get; set; }
        public BlockPoints points { get; set; }
        public List<string> series_ids { get; set; }
        public string scroll_id { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", Points: {points?.dates?.Count.ToString() ?? "-"}, Fields: {fields?.fields?.Count.ToString() ?? "-"}";
        }
    }

    public class BlockFields
    {
        public List<string> fields { get; set; }
        public List<List<object>> values { get; set; }
    }

    public class BlockPoints
    {
        public List<double> dates { get; set; }
        public List<List<double?>> values { get; set; }
    }

    public class SeriesPutRequest
    {
        public dynamic fields { get; set; }
        public Dictionary<double, double?> points { get; set; }
    }

    public class CreateJobRequest
    {
        public string description { get; set;}
        public string source { get; set; }
        public string notes { get; set; }   
    }

    public class SearchSeriesResponse : Response
    {
        public List<Dictionary<string, string>> hits { get; set; }
    }
}
