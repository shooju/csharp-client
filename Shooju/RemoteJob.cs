﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;

using TimeZoneConverter;

using Shooju.Exceptions;
using Shooju.Model;
using Shooju.Utils;

namespace Shooju
{
    /// <summary>
    /// Remote job class to manage all writing to Shooju through the job.
    /// </summary>
    public class RemoteJob
    {
        public int JobId { get; internal set; }

        private readonly Client _client;
        private readonly int _batchSize;
        private readonly int _maxPointsPerBatch;

        private int _currentPointsInBatch;

        private readonly Dictionary<Tuple<string, DateTime?>, SubmitRequest> _values =
            new Dictionary<Tuple<string, DateTime?>, SubmitRequest>();
        private readonly HashSet<string> _deleteSeriesIds = new HashSet<string>();
        readonly Dictionary<string, DeleteRequest> _deleteSeriesQueries = new Dictionary<string, DeleteRequest>();

        internal RemoteJob(Client client, string description, string source, int batchSize, int maxPointsPerBatch)
        {
            _client = client;
            var response = _client.CreateJob(description, source);
            response.CheckError();
            JobId = response.job_id;
            _maxPointsPerBatch = maxPointsPerBatch;
            _batchSize = batchSize;

            ResetBatch();
        }

        private void ResetBatch()
        {
            _currentPointsInBatch = 0;
            _values.Clear();
            _deleteSeriesIds.Clear();
            _deleteSeriesQueries.Clear();
            _seriesType = null;
        }

        /// <summary>
        /// Deprecated. Use Write() instead.  Buffers a series fields write.  The write happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="fields">fields to write</param>
        [Obsolete]
        public void PutFields(string seriesId, IEnumerable<KeyValuePair<string, dynamic>> fields)
        {
            Write(seriesId, SeriesIdentifierType.Id, null, fields, null, RemovalSwitch.None, null);
        }

        /// <summary>
        /// Deprecated. Use Write() instead.  Buffers a series points write.  The write happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="points">list of Points</param>
        [Obsolete]
        public void PutPoints(string seriesId, IReadOnlyCollection<Point> points)
        {
            Write(seriesId, SeriesIdentifierType.Id, null, null, points, RemovalSwitch.None, null);
        }

        /// <summary>
        /// Deprecated. Use WriteReported() instead.  Buffers a series points write.  The write happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="seriesId">series id</param>
        /// <param name="reportedDate">reported date</param>
        /// <param name="points">list of Points</param>
        [Obsolete]
        public void PutReportedPoints(string seriesId, DateTime reportedDate, IReadOnlyCollection<Point> points)
        {
            Write(seriesId, SeriesIdentifierType.Id, reportedDate, null, points, RemovalSwitch.None, null);
        }

        private void Write(string series, SeriesIdentifierType queryType, DateTime? reportedDate,
            IEnumerable<KeyValuePair<string, dynamic>> fields, IReadOnlyCollection<Point> points, RemovalSwitch removeOthers, string timezone)
        {
            if (fields == null && points == null)
                throw new ArgumentException("At least one of Fields or Points arguments must be specified");

            ValidateSeriesType(queryType);

            RemoveDeleteSeriesId(series); // don't delete series if were going to previously

            var convertedPoints = CheckTimezone(timezone, points);

            var seriesData = InitSeries(series, queryType, reportedDate);

            if (fields != null)
            {
                foreach (var keyVal in fields)
                {
                    seriesData.Fields[keyVal.Key] = keyVal.Value;
                }
            }

            if (points != null)
            {
                foreach (var point in convertedPoints)
                {
                    seriesData.Points[ConversionUtil.DateTimeToUnixTimestamp(point.Date)] = point.Value;
                }

                _currentPointsInBatch += convertedPoints.Count;
            }

            if (reportedDate != null)
            {
                seriesData.ReportedDate = ConversionUtil.DateTimeToUnixTimestamp(reportedDate.Value);
            }

            if (removeOthers != RemovalSwitch.None)
                seriesData.ReplaceItems |= removeOthers;

            CheckBufferAndSubmit();
        }

        /// <summary>
        /// Buffers a series write. Identifies the series using seriesQuery (must return 0 or 1 series; writing to more than one series is not supported).  The write happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="seriesQuery">series id</param>
        /// <param name="fields">fields to write</param>
        /// <param name="points">list of Points</param>
        /// <param name="removeOthers">remove data that already exists</param>
        /// <param name="timezone">IANA timezone</param>
        public void Write(string seriesQuery, IEnumerable<KeyValuePair<string, dynamic>> fields = null, IReadOnlyCollection<Point> points = null,
            RemovalSwitch removeOthers = RemovalSwitch.None, string timezone = null)
        {
            Write(seriesQuery, SeriesIdentifierType.Query, null, fields, points, removeOthers, timezone);
        }

        /// <summary>
        /// Buffers a series reported fields/points write. Identifies the series using seriesQuery (must return 0 or 1 series; writing to more than one series is not supported).  The write happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="seriesQuery">series query</param>
        /// <param name="reportedDate">reported date</param>
        /// <param name="fields">fields to write</param>
        /// <param name="points">points to write</param>
        public void WriteReported(string seriesQuery, DateTime reportedDate, IEnumerable<KeyValuePair<string, dynamic>> fields = null,
            IReadOnlyCollection<Point> points = null, string timezone = null)
        {
            Write(seriesQuery, SeriesIdentifierType.Query, reportedDate, fields, points, RemovalSwitch.None, timezone);
        }

        /// <summary>
        /// Deletes series by a query. Deletion with one=true is buffered and happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob, and deletion with one=false is executed immediately.
        /// </summary>
        /// <param name="query">series query</param>
        /// <param name="one">specifies whether the query is expected to delete just one series</param>
        /// <param name="force">specifies if series should be moved to “trash“, or permanently deleted</param>
        public int DeleteSeries(string query, bool one = true, bool force = false)
        {
            if (one)
            {
                var request = new DeleteRequestSingle
                {
                    Query = query,
                    Force = force,
                    JobId = JobId,
                };
                AddDeleteSeriesQuery(query, request);
                CheckBufferAndSubmit();
                return 0;
            }
            else
            {
                var request = new DeleteRequestMulti
                {
                    Query = query,
                    Force = force,
                    JobId = JobId,
                };
                return _client.Delete(request);
            }
        }

        /// <summary>
        /// Deletes data by a series query and reported date. Deletion is buffered and happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="query">series query</param>
        /// <param name="reportedDates">reported date</param>
        public void DeleteReported(string query, params DateTime[] reportedDates)
        {
            var request = new DeleteRequestReported
            {
                Query = query,
                JobId = JobId,
            };
            request.ReportedDates.AddRange(reportedDates.Select(ConversionUtil.DateTimeToUnixTimestamp));

            var response = _client.Post<ResponseDeleted>(_client.SERIES_DELETE_REPORTED_ENDPOINT, request);
            response.CheckError();
        }

        /// <summary>
        /// Deprecated. Buffers a series deletion.  The delete happens only on .Submit() or depending on the batchSize and maxPointsPerBatch settings of the RemoteJob.
        /// </summary>
        /// <param name="seriesId">series id</param>
        [Obsolete]
        public void Delete(string seriesId)
        {
            AddDeleteSeriesId(seriesId);

            CheckBufferAndSubmit();
        }

        /// <summary>
        /// Deprecated. Deletes series by a query.  The delete happens immediately (ignoring batchSize and maxPointsPerBatch settings of the RemoteJob).
        /// </summary>
        /// <param name="query">query</param>
        [Obsolete]
        public int DeleteByQuery(string query)
        {
            var body = new
            {
                query,
                job_id = JobId.ToString(),
            };
            return _client.Delete(body);
        }

        /// <summary>
        /// Deprecated. Use Write() instead.  Removes all fields of a series other than the ones written immediately afterwards.  Useful only in conjunction with PutFields().
        /// </summary>
        /// <param name="seriesId">series id</param>
        [Obsolete]
        public void RemoveFields(string seriesId)
        {
            var seriesData = InitSeries(seriesId, SeriesIdentifierType.Id);

            seriesData.ReplaceItems |= RemovalSwitch.Fields;
        }

        /// <summary>
        /// Deprecated. Use Write() instead.  Removes all points of a series other than the ones written immediately afterwards.  Useful only in conjunction with PutPoints().
        /// </summary>
        /// <param name="seriesId">series id</param>
        [Obsolete]
        public void RemovePoints(string seriesId)
        {
            var seriesData = InitSeries(seriesId, SeriesIdentifierType.Id);

            seriesData.ReplaceItems |= RemovalSwitch.Points;
        }

        private SubmitRequest InitSeries(string series, SeriesIdentifierType queryType, DateTime? reportedDate = null)
        {
            ValidateSeriesType(queryType);

            var key = Tuple.Create(series, reportedDate);

            if (!_values.ContainsKey(key))
            {
                SubmitRequest seriesData;
                switch (queryType)
                {
                    case SeriesIdentifierType.Id:
                        seriesData = new SubmitRequestById { Id = series };
                        break;
                    case SeriesIdentifierType.Query:
                        seriesData = new SubmitRequestByQuery { Query = series };
                        break;
                    default:
                        throw new NotSupportedException();
                }

                seriesData.ReportedDate = reportedDate == null
                    ? (long?)null
                    : ConversionUtil.DateTimeToUnixTimestamp(reportedDate.Value);

                _values[key] = seriesData;
            }

            return _values[key];
        }

        private void CheckBufferAndSubmit()
        {
            if (_values.Count + _deleteSeriesIds.Count + _deleteSeriesQueries.Count >= _batchSize || _currentPointsInBatch >= _maxPointsPerBatch)
            {
                Submit();
            }
        }

        /// <summary>
        /// Writes all buffered requests immediately.
        /// </summary>
        public void Submit()
        {
            var requests = new List<ISubmitRequest>(_values.Values);
            requests.AddRange(_deleteSeriesQueries.Values);
            requests.AddRange(_deleteSeriesIds.Select(id => new DeleteRequestById { Id = id }));

            if (requests.Count == 0)
                return;

            try
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["job_id"] = JobId.ToString();
                query["date_format"] = "milli";

                var bulkResponse = _client.BulkSubmit(requests, query);

                //check results

                if (requests.Count != bulkResponse.Responses.Count)
                    throw new ShoojuClientException("The number of requests and the number of responses don't match");

                var errorText = new StringBuilder();
                List<RequestResponse> responses = null;

                var i = 0;
                foreach (var response in bulkResponse.Responses)
                {
                    if (!response.Success)
                    {
                        var curError = response.FormatError();
                        errorText.Append($"{requests[i].IdOrQuery()}: {curError}", ", ");
                        if (responses == null)
                            responses = new List<RequestResponse>();
                        responses.Add(new RequestResponse(i, requests[i], bulkResponse.Responses[i]));
                    }

                    i++;
                }

                if (errorText.Length > 0)
                    throw new ShoojuClientException(errorText.ToString(), responses: responses);
            }
            finally
            {
                // always reset the cache
                ResetBatch();
            }
        }

        void AddDeleteSeriesId(string seriesId)
        {
            ValidateSeriesType(SeriesIdentifierType.Id);
            _deleteSeriesIds.Add(seriesId);
        }

        void RemoveDeleteSeriesId(string seriesId)
        {
            _deleteSeriesIds.Remove(seriesId);
        }

        void AddDeleteSeriesQuery(string query, DeleteRequest request)
        {
            ValidateSeriesType(SeriesIdentifierType.Query);
            _deleteSeriesQueries[query] = request;
        }

        /// <summary>
        /// Marks a job as finished.  This should be called on all jobs upon completion.
        /// </summary>
        public void Finish()
        {
            Submit();
            var finishResponse = _client.FinishJob(JobId);
        }

        void ValidateSeriesType(SeriesIdentifierType type)
        {
            if (_seriesType == null)
            {
                _seriesType = type;
            }
            else
            {
                // if series data of other type is present in the cache, flush it immediately
                if (_seriesType.Value != type)
                {
                    Trace.WriteLine("WARNING - SJPutPoints and SJPutFields functions are obsolete.");
                    Submit();
                }
            }
        }

        IReadOnlyCollection<Point> CheckTimezone(string timezone, IReadOnlyCollection<Point> points)
        {
            if (!string.IsNullOrEmpty(timezone))
            {
                var convertedPoints = new List<Point>(points);

                string windowsTimeZoneId = TZConvert.IanaToWindows(timezone);
                TimeZoneInfo tzInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZoneId);
                //return TimeZoneInfo.ConvertTimeToUtc(dateTime, tzInfo);

                foreach (var point in convertedPoints)
                {
                    point.Date = TimeZoneInfo.ConvertTimeToUtc(point.Date, tzInfo);
                }

                return convertedPoints;
            }

            return points;
        }

        SeriesIdentifierType? _seriesType = null;

        enum SeriesIdentifierType
        {
            Id,
            Query,
        }
    }
}
