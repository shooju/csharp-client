﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using Shooju.Model;

namespace Shooju.Exceptions
{
    public class ShoojuClientException : ApplicationException
    {
        public ShoojuClientException(bool serverError = true)
        {
            ServerError = serverError;
        }

        public ShoojuClientException(string message, bool serverError)
            : base(message)
        {
            ServerError = serverError;
            LoginError = IsLoginError(message);
        }

        public ShoojuClientException(string message, Exception innerException = null, List<RequestResponse> responses = null, bool serverError = true)
            : base(message, innerException)
        {
            ServerError = serverError;
            LoginError = IsLoginError(message);
            Responses = responses;
        }

        protected ShoojuClientException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        static bool IsLoginError(string message)
        {
            return message.Contains("invalid_credentials: Invalid 'user' and/or 'api_secret'");
        }

        public readonly bool ServerError;
        public readonly bool LoginError;

        public readonly List<RequestResponse> Responses;
    }

    public class RequestResponse
    {
        public RequestResponse(int index, IShoojuRequest request, Response response)
        {
            Index = index;
            Request = request;
            Response = response;
        }

        public readonly int Index;
        public readonly IShoojuRequest Request;
        public readonly Response Response;
    }
}
