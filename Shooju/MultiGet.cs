﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Shooju.Model;

namespace Shooju
{
    /// <summary>
    /// Retrieves multiple field and/or points
    /// </summary>
    public class MultiGet
    {
        private Client client;
        private List<RequestPost> requests;

        /// <summary>
        /// Creates a MultiGet instance, needs a configured Client instance. Should not be used by itself, it's better
        /// to retrieve a configured instance from Connection.GetMultGet method.
        /// </summary>
        /// <param name="client">configured client</param>
        internal MultiGet(Client client)
        {
            this.client = client;
            this.requests = new List<RequestPost>();
        }

        /// <summary>
        /// Retrieves points for a given query (using the new POST API)
        /// </summary>
        /// <param name="seriesQuery">query that returns 1 series</param>
        /// <param name="fields">list of fields to retrieve; use * for all non-meta</param>
        /// <param name="df">date FROM for points; can be DateTime, 'MAX', 'MIN', or relative date format</param>
        /// <param name="dt">date TO for points; can be DateTime, 'MAX', 'MIN', or relative date format</param>
        /// <param name="maxPoints">number of points to retrieve per series; use -1 for all</param>
        /// <param name="extraParams">extra parameters; refer to API Documentationt</param>
        public int GetSeries(string seriesQuery, IList<string> fields = null, object df = null, object dt = null, int maxPoints = 0,
            Dictionary<string, object> extraParams = null)
        {
            var request = new RequestPost
            {
                DateFrom = df,
                DateTo = dt,
                Query = seriesQuery,
                Fields = fields,
                MaxPoints = maxPoints,
                ExtraParams = extraParams,
            };

            AddSeriesRequest(request);

            return GetTicket();
        }

        /// <summary>
        /// Fetches the requests from shooju, returns a list of series with the data
        /// from each request in the order they were requested. The Series class has attributes Fields, 
        /// Points and SeriesId. Depending on the request either Fields or Points will be null. In the case the
        /// series does not exist or the server had problems processing the request both Fields and Points will be
        /// null.
        /// </summary>
        /// <returns>list of series</returns>
        public SeriesResponseList Fetch(Dictionary<string, object> extraParams = null)
        {
            var pendingRequests = requests.ToArray();
            var bulkResponse = client.GetSeries(extraParams, pendingRequests);
            Clear();

            if (!bulkResponse.CheckError())
                return null;

            return new SeriesResponseList(bulkResponse.Series, bulkResponse.RawResponse);
        }

        /// <summary>
        /// Clears the request queue.
        /// </summary>
        public void Clear()
        {
            this.requests.Clear();
        }

        private void AddSeriesRequest(RequestPost request)
        {
            requests.Add(request);
        }

        private int GetTicket()
        {
            return this.requests.Count - 1;
        }
    }
}
