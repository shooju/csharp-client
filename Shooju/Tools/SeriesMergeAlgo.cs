﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Shooju.Model;

namespace Shooju.Tools
{
    public static class SeriesMergeAlgo
    {
        public static List<object[]> Merge(IList<Series> seriesList, bool reverseOrder = false)
        {
            var comparer = reverseOrder
                ? Comparer<DateTime>.Create((x, y) => y.CompareTo(x))
                : Comparer<DateTime>.Create((x, y) => x.CompareTo(y));

            foreach (var series in seriesList)
            {
                if (series.Points == null || series.Points.Count == 0)
                    continue;
                if (comparer.Compare(series.Points.First().Date, series.Points.Last().Date) > 0)
                    throw new ArgumentException();
            }

            var indexes = new int[seriesList.Count];

            var dstIndexes = FindDstIndexes(seriesList, indexes, reverseOrder, comparer);
            var dstActive = dstIndexes.Any(cur => cur >= 0);

            var res = new List<object[]>();
            var maxDate = reverseOrder ? DateTime.MinValue : DateTime.MaxValue;

            while (true)
            {
                if (dstActive)
                {
                    var dstComplete = true;
                    for (var i = 0; i < seriesList.Count; i++)
                    {
                        if (indexes[i] < dstIndexes[i])
                            dstComplete = false;
                    }

                    if (dstComplete)
                    {
                        dstIndexes = FindDstIndexes(seriesList, indexes, reverseOrder, comparer);
                        dstActive = dstIndexes.Any(cur => cur >= 0);
                    }
                }

                var curDate = maxDate;

                IterateThroughSeries(seriesList, indexes, dstIndexes,
                    (point, i) =>
                    {
                        if (comparer.Compare(point.Date, curDate) < 0)
                            curDate = point.Date;
                    });

                if (curDate == maxDate)
                    break;

                var line = new object[seriesList.Count + 1];
                line[0] = curDate;

                IterateThroughSeries(seriesList, indexes, dstIndexes,
                    (point, i) =>
                    {
                        if (comparer.Compare(point.Date, curDate) > 0)
                            return;

                        line[i + 1] = point.Value;
                        indexes[i]++;
                    });

                res.Add(line);
            }

            return res;
        }

        private static int[] FindDstIndexes(IList<Series> seriesList, int[] indexes, bool reverseOrder, IComparer<DateTime> comparer)
        {
            var res = Enumerable.Repeat(-1, seriesList.Count).ToArray();

            var minDate = reverseOrder ? DateTime.MaxValue : DateTime.MinValue;
            DateTime? curDate = null;

            var i = 0;
            foreach (var series in seriesList)
            {
                var prevDate = minDate;

                if (series.Points != null)
                {
                    for (var j = indexes[i]; j < series.Points.Count; j++)
                    {
                        var point = series.Points[j];

                        if (comparer.Compare(point.Date, prevDate) <= 0 &&
                            (curDate == null || comparer.Compare(point.Date, curDate.Value) == 0))
                        {
                            curDate = point.Date;
                            res[i] = j;
                            break;
                        }

                        prevDate = point.Date;
                    }
                }

                i++;
            }

            if (curDate == null)
                return res;

            i = 0;
            foreach (var series in seriesList)
            {
                if (res[i] >= 0)
                    continue;

                if (series.Points != null)
                {
                    for (var j = indexes[i]; j < series.Points.Count; j++)
                    {
                        var point = series.Points[j];

                        var timeDiff = (point.Date - curDate.Value).TotalHours;
                        if (reverseOrder)
                            timeDiff = -timeDiff;

                        if (timeDiff >= 1)
                        {
                            res[i] = j;
                            break;
                        }
                    }
                }

                i++;
            }

            return res;
        }

        private static void IterateThroughSeries(IList<Series> seriesList, int[] indexes, int[] dstIndexes, Action<Point, int> action)
        {
            for (var i = 0; i < seriesList.Count; i++)
            {
                var series = seriesList[i];

                if (series.Points != null)
                {
                    var index = indexes[i];
                    if (index >= series.Points.Count)
                        continue;

                    if (indexes[i] == dstIndexes[i])
                        continue;

                    var point = series.Points[index];
                    action(point, i);
                }
            }
        }

        public static object[,] ToMatrix(this IReadOnlyList<object[]> vals)
        {
            var maxItemLength = vals.Count > 0
                ? vals.Max(cur => cur.Length)
                : 0;
            var res = new object[vals.Count, maxItemLength];

            var i = 0;
            foreach (var row in vals)
            {
                var j = 0;
                foreach (var cur in row)
                {
                    res[i, j] = cur ?? "";
                    j++;
                }

                for (; j < maxItemLength; j++)
                {
                    res[i, j] = "";
                }

                i++;
            }

            return res;
        }
    }
}
