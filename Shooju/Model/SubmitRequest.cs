﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class SubmitRequest : IShoojuRequest, ISubmitRequest
    {
        [JsonIgnore]
        public Dictionary<long, double?> Points = new Dictionary<long, double?>();

        [JsonProperty(PropertyName = "points")]
        public List<object[]> PointsSerialized =>
            Points.Select(tuple => new object[] { tuple.Key, tuple.Value }).ToList();

        [JsonProperty(PropertyName = "fields")]
        public Dictionary<string, object> Fields = new Dictionary<string, object>();

        [JsonProperty(PropertyName = "reported_date")]
        public long? ReportedDate;

        [JsonIgnore]
        public RemovalSwitch ReplaceItems = RemovalSwitch.None;

        [JsonProperty(PropertyName = "keep_only")]
        public string KeepOnly => (ReplaceItems == RemovalSwitch.None)
            ? null
            : ReplaceItems.ToString().ToLowerInvariant();

        public override string ToString()
        {
            return $"Points: {Points.Count} Fields: {Fields.Count} ReplaceItems: {ReplaceItems}";
        }
    }

    [Flags]
    public enum RemovalSwitch
    {
        None = 0,
        Points = 1,
        Fields = 2,
        All = Points | Fields,
    }
}
