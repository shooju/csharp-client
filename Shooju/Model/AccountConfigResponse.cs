﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class AccountConfigResponse : Response
    {
        [JsonProperty("account_config")]
        public readonly AccountConfig AccountConfig = new AccountConfig();
    }

    public class AccountConfig
    {
        [JsonProperty("excel_load_dlls")]
        public readonly List<string> ExcelLoadDlls = new List<string>();

        [JsonProperty("excel_load_cabs")]
        public readonly List<string> ExcelLoadCabs = new List<string>();
    }
}
