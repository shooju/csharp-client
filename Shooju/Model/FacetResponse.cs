﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class FacetResponse : Response
    {
        [JsonProperty(PropertyName = "facets")]
        public Dictionary<string, FacetGroup> Facets { get; set; }

        public override string ToString()
        {
            return $"Facets: {Facets.Count}";
        }
    }
}
