﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

using Shooju.Utils;

namespace Shooju.Model
{
    public class RequestBase : IShoojuRequest
    {
        /// <summary>
        /// Query for searching series
        /// </summary>
        [JsonProperty(PropertyName = "query")]
        public string Query { get; set; }

        /// <summary>
        /// List of fields to retrieve;
        /// </summary>
        [JsonIgnore]
        public IList<string> Fields { get; set; }

        /// <summary>
        /// Ignore fields list and retrieve all fields
        /// </summary>
        [JsonIgnore]
        public bool AllFields = false;

        [JsonProperty(PropertyName = "fields")]
        public string FieldsSerialized
        {
            get
            {
                if (AllFields)
                    return "*";

                if (Fields == null)
                    return null;

                if (Fields.Count == 0)
                    return null;

                return string.Join(",", Fields);
            }
        }

        /// <summary>
        /// Start date
        /// </summary>
        [JsonIgnore]
        public object DateFrom { get; set; }

        [JsonProperty(PropertyName = "df")]
        public string DateFromSerialized =>
            SerializeDate(DateFrom, "MIN");

        public static string SerializeDate(object date, string defaultVal = null)
        {
            if (date == null)
                return defaultVal;

            var strVal = date as string;
            if (strVal != null)
                return strVal;

            return ConversionUtil.DateToString((DateTime)date, defaultVal);
        }

        /// <summary>
        /// End date
        /// </summary>
        [JsonIgnore]
        public object DateTo { get; set; }

        [JsonProperty(PropertyName = "dt")]
        public string DateToSerialized =>
            SerializeDate(DateTo, "MAX");

        /// <summary>
        /// Max number of pointer per series to retrieve
        /// </summary>
        [JsonProperty(PropertyName = "max_points")]
        public int MaxPoints { get; set; }

        public override string ToString()
        {
            return $"Query: {Query}, DateFrom: {DateFrom}, DateTo: {DateTo}, MaxPoints: {MaxPoints}, Fields: {Fields}";
        }

        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            Converters =
            {
                new PointConverter(),
            },
        };
    }
}
