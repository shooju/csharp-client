﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

using Shooju.Utils;

namespace Shooju.Model
{
    public class RequestPost : RequestBase
    {
        [JsonExtensionData]
        public Dictionary<string, object> ExtraParams;

        public override string ToString()
        {
            var extraParamsText = (ExtraParams == null)
                ? ""
                : $", ExtraParams: {ExtraParams}";
            return $"{base.ToString()}{extraParamsText}";
        }
    }
}
