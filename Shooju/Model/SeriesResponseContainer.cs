﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class SeriesResponseContainer : Response
    {
        [JsonProperty(PropertyName = "series")]
        public List<Series> Series { get; set; }

        [JsonExtensionData]
        public Dictionary<string, object> RawResponse;

        public override string ToString()
        {
            return base.ToString() + $", Responses: {Series?.Count.ToString() ?? "-"}";
        }
    }
}
