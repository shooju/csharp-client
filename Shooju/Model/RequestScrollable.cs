﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class RequestScrollable : Request
    {
        public int ScrollBatchSize { get; set; } = DefaultScrollBatchSize;

        public const int DefaultScrollBatchSize = 500;

        public Dictionary<string, object> ExtraParams;

        public int? MaxSeries { get; set; } = null;

        public bool WasScrolled = true;

        public override string ToString()
        {
            var extraParamsText = (ExtraParams == null)
                ? ""
                : $", ExtraParams: {ExtraParams}";
            return $"{base.ToString()}{extraParamsText}";
        }
    }
}
