﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class Series : Response
    {
        [JsonProperty(PropertyName = "series_id")]
        public string SeriesId { get; set; }

        [JsonProperty(PropertyName = "points")]
        public List<Point> Points { get; set; }

        [JsonProperty(PropertyName = "fields")]
        public Dictionary<string, object> Fields { get; } = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        [JsonIgnore]
        public object Tag;

        [JsonExtensionData]
        public Dictionary<string, object> More;

        public override string ToString()
        {
            return base.ToString() + $", Series: \"{SeriesId}\", Points: {Points?.Count.ToString() ?? "-"}, Fields: {Fields?.Count.ToString() ?? "-"}";
        }
    }
}
