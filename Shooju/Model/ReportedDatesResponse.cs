﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Shooju.Utils;

namespace Shooju.Model
{
    public class ReportedDatesResponse : Response
    {
        [JsonProperty(PropertyName = "reported_dates")]
        public List<long> ReportedDatesMilli;

        public List<DateTime> ReportedDates
        {
            get
            {
                var reportedDates = new List<DateTime>();
                foreach (var repMilli in ReportedDatesMilli)
                {
                    reportedDates.Add(ConversionUtil.UnixTimeStampToDateTime(repMilli));
                }

                return reportedDates;
            }
        }
    }
}