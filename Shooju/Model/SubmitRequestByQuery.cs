﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    class SubmitRequestByQuery : SubmitRequest
    {
        [JsonProperty("query")]
        public string Query { get; set; }
    }
}
