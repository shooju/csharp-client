﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Shooju.Model
{
    class SessionRenewResponse : Response
    {
        [JsonProperty(PropertyName = "api_key")]
        public string ApiKey;
    }
}
