﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    class DeleteRequest : ISubmitRequest
    {
        [JsonProperty("force")]
        public bool? Force { get; set; }

        [JsonProperty("job_id")]
        public int? JobId { get; set; }

        [JsonProperty("type")]
        public readonly string Type = "DELETE";
    }

    class DeleteRequestSingle : DeleteRequest
    {
        [JsonProperty("series_query")]
        public string Query { get; set; }
    }

    class DeleteRequestMulti : DeleteRequest
    {
        [JsonProperty("query")]
        public string Query { get; set; }
    }

    class DeleteRequestReported : DeleteRequestSingle
    {
        [JsonProperty("reported_dates")]
        public readonly List<long> ReportedDates = new List<long>();
    }

    class DeleteRequestById : ISubmitRequest
    {
        [JsonProperty("series_id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public readonly string Type = "DELETE";
    }
}
