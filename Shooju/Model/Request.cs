﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    /// <summary>
    /// Base request arguments class for "/series" requests
    /// </summary>
    public class Request : RequestBase
    {
        /// <summary>
        /// Series ID; mutually exclusive with Query
        /// </summary>
        [JsonProperty(PropertyName = "series_id")]
        public string SeriesId { get; set; }

        /// <summary>
        /// Extra operators for the query.
        /// </summary>
        [JsonProperty(PropertyName = "operators")]
        public string Operators { get; set; }

        /// <summary>
        /// List of names to sort on
        /// </summary>
        [JsonIgnore]
        public string SortOn { get; set; }

        [JsonProperty(PropertyName = "sort")]
        public string SortOnSerialized
        {
            get
            {
                if (SortOn == null)
                    return null;
                if (SortOn.Length == 0)
                    return "series_id asc";
                return SortOn;
            }
        }

        public override string ToString()
        {
            return $"Series: {SeriesId},{base.ToString()}";
        }
    }
}
