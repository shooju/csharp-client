﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooju.Model
{
    interface ISubmitRequest : IShoojuRequest
    {
    }

    static class SubmitRequestFuncs
    {
        public static string IdOrQuery(this ISubmitRequest request)
        {
            switch (request)
            {
                case DeleteRequestSingle delete:
                    return delete.Query;
                case DeleteRequestById delete:
                    return delete.Id;
                case SubmitRequestById byId:
                    return byId.Id;
                case SubmitRequestByQuery byQuery:
                    return byQuery.Query;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
