﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class Facet
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "term")]
        public string Term { get; set; }

        [JsonProperty(PropertyName = "sub")]
        public FacetGroup Sub
        {
            get { return _sub; }
            set
            {
                if (value.Value == null)
                {
                    _sub = value;
                }
                else
                {
                    _sub = null;
                    Value = value.Value;
                }
            }
        }
        private FacetGroup _sub;

        public object Value { get; set; }

        public override string ToString()
        {
            var subText = "";
            if (Sub != null)
                subText = $", Sub: '{Sub}'";
            var valText = "";
            if (Value != null)
                valText = $", Value:{Value}";
            return $"Term: {Term}, Count: {Count}{valText}{subText}";
        }
    }
}
