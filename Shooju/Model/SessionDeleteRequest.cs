﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Shooju.Model
{
    class SessionDeleteRequest
    {
        [JsonProperty(PropertyName = "agent")]
        public string Agent;
    }
}
