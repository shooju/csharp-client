﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    class MultiPartUploadResponse : Response
    {
        [JsonProperty(PropertyName = "file_id")]
        public string FileId;
    }
}
