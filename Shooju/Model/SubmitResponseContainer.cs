﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class SubmitResponseContainer : Response
    {
        [JsonProperty(PropertyName = "responses")]
        public List<Series> Responses { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", Responses: {Responses?.Count.ToString() ?? "-"}";
        }
    }
}
