﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Shooju.Exceptions;

namespace Shooju.Model
{
    public class SeriesResponseEnumerable : IEnumerable<Series>
    {
        private readonly RequestScrollable _request;
        private readonly SeriesResponseContainer _firstScrollResponse;
        private readonly Client _client;

        /// <summary>
        /// Root API response.  Useful for retrieving additional parameters outside of series from the API response.
        /// </summary>
        public Dictionary<string, object> RootResponse;

        /// <summary>
        /// Maximum number of series that was requested; null for no limit.
        /// </summary>
        public int? RequestedMaxSeries;

        internal SeriesResponseEnumerable(Client client, Request request, int? maxSeries = null)
        {
            _client = client;
            RequestedMaxSeries = maxSeries;
            _request = request as RequestScrollable;

            _firstScrollResponse = _client.GetSeries(request);

            if (!_firstScrollResponse.Success)
                throw new ShoojuClientException(_firstScrollResponse.Error + ": " + _firstScrollResponse.Description);
            if (_firstScrollResponse.Series == null)
                throw new ShoojuClientException("no results", false);

            RootResponse = _firstScrollResponse.RawResponse;
        }

        public IEnumerator<Series> GetEnumerator()
        {
            int count = 0;
            var response = _firstScrollResponse;
            while (true)
            {
                foreach (var series in response.Series)
                {
                    yield return series;
                    count++;
                    if (count >= RequestedMaxSeries)
                        yield break;
                }

                if (response.Series.Count == 0 ||
                    (RequestedMaxSeries != null && count >= RequestedMaxSeries) ||
                    count >= Convert.ToInt32(_firstScrollResponse.RawResponse["total"]) ||
                    _request == null ||
                    RequestedMaxSeries <= _request.ScrollBatchSize ||
                    _request.WasScrolled == false)
                {
                    yield break;
                }

                var scrollId = response.RawResponse["scroll_id"] as string;
                response = _client.Scroll(scrollId);
                response.CheckError();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
