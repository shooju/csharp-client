﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    class ResponseDeleted : Response
    {
        [JsonProperty(PropertyName = "deleted")]
        public int Deleted { get; set; }
    }
}
