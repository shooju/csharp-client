﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Shooju.Model
{
    class SessionRenewRequest
    {
        [JsonProperty(PropertyName = "agent")]
        public string Agent;

        [JsonProperty(PropertyName = "expiration")]
        public double Expiration;
    }
}
