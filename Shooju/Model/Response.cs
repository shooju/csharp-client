﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

using Shooju.Exceptions;
using Shooju.Utils;

namespace Shooju.Model
{
    public class Response
    {
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public bool Success =>
            string.IsNullOrEmpty(Error);

        public bool NotFoundError { get; private set; }

        public override string ToString()
        {
            return $"Success: {Success}, Error: \"{Error}\", Description: \"{Description}\"";
        }

        public bool CheckError()
        {
            if (!Success)
            {
                if (Error.Equals("series_not_found"))
                {
                    NotFoundError = true;
                    return false;
                }

                throw new ShoojuClientException(FormatError());
            }

            return true;
        }

        public string FormatError()
        {
            if (Success)
                throw new InvalidOperationException();

            if (Description.IsEmpty())
                return Error;
            else
                return $"{Error}: {Description}";
        }
    }
}
