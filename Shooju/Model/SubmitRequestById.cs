﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class SubmitRequestById : SubmitRequest
    {
        [JsonProperty("series_id")]
        public string Id { get; set; }
    }
}
