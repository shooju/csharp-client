﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

namespace Shooju.Model
{
    public class FacetGroup
    {
        [JsonProperty(PropertyName = "total")]
        public int Total { get; set; }

        [JsonProperty(PropertyName = "other")]
        public int Other { get; set; }

        [JsonProperty(PropertyName = "missing")]
        public int Missing { get; set; }

        [JsonProperty(PropertyName = "terms")]
        public List<Facet> Terms { get; set; }

        [JsonProperty(PropertyName = "value")]
        public object Value;

        public override string ToString()
        {
            return $"Total: {Total}, Terms: {Terms.Count}";
        }
    }
}
