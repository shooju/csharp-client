﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shooju.Model
{
    public class Point
    {
        public Point(DateTime date, double? value)
        {
            this.Value = value;
            this.Date = date;
        }

        public double? Value { get; set; }

        public DateTime Date { get; set; }

        public UInt32? JobId { get; set; }

        public DateTime? Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var that = obj as Point;
            if (that == null)
                return false;

            var res = Date.Equals(that.Date) && Equals(Value, that.Value);
            return res;
        }

        public override string ToString()
        {
            return $"{Date} -> {Value}";
        }
    }
}
