﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shooju.Model
{
    public class SeriesResponseList : List<Series>
    {
        public SeriesResponseList(IList<Series> data, Dictionary<string, object> rootResponse)
            : base(data)
        {
            RootResponse = rootResponse;
        }

        public Dictionary<string, object> RootResponse;
    }
}
